package xyz.cruxlab.pocketmoney_edited.ads;

import android.content.Context;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;

/**
 * Created by sudarshan on 9/7/2016.:)
 */
public class LoadService {

    static public void LoadAds(Context context) {

        final InterstitialAd mInterstitialAd;
        mInterstitialAd = new InterstitialAd(context);

        LocalData localData = new LocalData(context);
        mInterstitialAd.setAdUnitId(localData.getAds1());

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();


                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                //Toast.makeText(context, "Failed to load Ads.\nTry again.", Toast.LENGTH_SHORT).show();

            }

//            @Override
//            public void onAdClosed() {
//                super.onAdClosed();
//                //Ads closed
//
//
//            }
        });
    }

//    static public void LoadAdsExtra(Context context) {
//
//
//        interstitialAd = new InterstitialAd(context);
//        interstitialAd.setAdUnitId(context.getString(R.string.interstitial));
//
//        AdRequest adRequest = new AdRequest.Builder()
//                .build();
//        interstitialAd.loadAd(adRequest);
//        interstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                Earning earning = new Earning();
//                earning.setNoticeText("Loaded");
//
//            }
//
//            @Override
//            public void onAdClosed() {
//                super.onAdClosed();
//                //Ads closed
//                Log.d(TAG, "Ads closed");
//                Earning earning = new Earning();
//                earning.setTimer30Sec();
//
//            }
//        });
//    }
//
//    public static void showAdsExtra() {
//
//        if (interstitialAd.isLoaded()) {
//            interstitialAd.show();
//        }else {
//            Log.d(TAG, "Ads not loaded. Failed");
//        }
//
//    }

}
