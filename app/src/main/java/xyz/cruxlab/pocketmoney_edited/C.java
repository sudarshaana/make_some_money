package xyz.cruxlab.pocketmoney_edited;

public class C {

    public static String GATE_WAY = "http://cruxlab.xyz/api/pm_v1_1/";


    public static String MAIN = "main.php";
    public static String LOGIN = "login.php";
    public static String UPDATE_PROFILE = "update_profile.php";
    public static String UPDATE_TOKEN = "update_token.php";
    public static String CK_POINT = "ck_point.php";
    public static String WITHDRAWAL_REQ = "withdrawal_req.php";
    public static String UPDATE_POINTS = "update_points_v2.php";
    public static String CK_PROFILE = "ck_profile.php";
    public static String REF_CODE = "ref_code.php";
    public static String REF_COUNT = "ref_count.php";
    public static String LEADER_BOARD = "leaderbooard.php";
    public static String WITHDRAWAL_HIST = "withdrawal_hist.php";

    public static String NOTICE_LIST = "notice_list.php";
    public static String NOTICE_DETAILS = "notice_details.php";

    public static String POINTS_FOR_PROFILE = "points_for_profile.php";
    public static String PROFILE_INFO = "profile_info.php";
    public static String DAILY_MAX_LIMIT = "insert_daily_limit.php";

    public static String FORGOT_PIN = "pin/forgot_pin.php";


    public static String IDENTIFIER = "identifier";
    public static String DAILY_SESSION_LIMIT = "daily_limit";
    public static String NAME = "name";
    public static String MESSAGE = "msg";
    public static String PHONE_NO = "phone_no";
    public static String FIREBASE_ID = "firebase_id";
    public static String USER_ID = "user_id";
    public static String DEVICE_NAME = "device_name";
    public static String POINTS = "points";
    public static String INDEX = "index";
    public static String SESSION = "session";
    public static String PIN = "pin";
    public static String WITHDRAW_METHOD = "withdrawl_method";
    public static String WITHDRAW_NO = "withdrawl_no";
    public static String NUMBER_WD = "number";
    public static String CURRENT_POINTS = "current_points";
    public static String TOTAL_POINTS = "total_point";
    public static String CONVERSION_RATE = "conversion_rate";
    public static String RECENT_WITHDRAWAL = "withdrawl";
    public static String REFERRAL_CODE = "ref_code";

    public static String NOTICE_ID = "notice_id";
    public static String NOTICE_TEXT = "notice_text";

    //
    public static String BADGE = "badge";
    public static String WITHDRAWAL_METHOD = "withdrawl_method";
    public static String AC_STATUS = "ac_status";


    public static String APP_CODE = "version";
    public static String APP_URL = "url";
    public static String UPDATE_TYPE = "update_type";
    public static String ACC_CREATED = "ac_created";


    public static String ADS1 = "ads1";
    public static String ADS2 = "ads1";


    public static String STATUS = "status";
    public static String REF_NO = "number";

    public static int STATUS_OKAY = 0;
    public static int STATUS_OLD_USER = 1;
    public static int STATUS_NO_Point = 2;
    public static int STATUS_REF_ALREADY_APPLIED = 3;
    public static int STATUS_OWN_REF_CODE = 4;

    public static int POINT_ALREADY_ADDED = 5;
    public static int MAX_POINT = 6;


    public static int STATUS_PIN_ERROR = -6;
    public static int STATUS_NOT_ENOUGH_POINT = -5;
    public static int STATUS_NEW_ACCOUNT = -4;
    public static int STATUS_INV_REF_CODE = -3;
    public static int STATUS_ACCOUNT_ERROR = -2;
    public static int STATUS_ERROR = -1;

    public static int ERROR_TOKEN_UPDATE = 9;
    public static int STATUS_SERVER_ERROR = 10;
    public static int ERROR_NEW_USER_LIMIT = 11;
    public static int ERROR_PAYMENT_LIMIT = 12;
    public static int ERROR_SESSION_RUNNING = 13;


}
