package xyz.cruxlab.pocketmoney_edited.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.classes.ProfileAdapter;
import xyz.cruxlab.pocketmoney_edited.classes.ProfileItem;
import xyz.cruxlab.pocketmoney_edited.classes.User;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.POINTS_FOR_PROFILE;
import static xyz.cruxlab.pocketmoney_edited.C.REFERRAL_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.REF_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_INV_REF_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OWN_REF_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_REF_ALREADY_APPLIED;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class EarningFragment extends Fragment {

    RecyclerView dailyPointRecy;
    View rootView;
    LocalData localData;
    TextView failedTextView;
    private int statusCode;
    Context context;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_earning, container, false);
        localData = new LocalData(getContext());
        context = getContext();




        if (Utils.isNetworkAvailable(getContext())) {
            new LoadDataFromServer().execute();
        } else {
            //failedTextView.setVisibility(View.VISIBLE);
        }





        return rootView;
    }

    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(getContext(), "Applying Ref Code", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl1 getData = new GetDataFromUrl1();
            String response = null;
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getContext()))
                        .build();

                response = getData.run(GATE_WAY + POINTS_FOR_PROFILE, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            //Log.d("statusResult", result);
            //Log.d("FuckPoint", result);

            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);

                    GraphView graph = (GraphView) rootView.findViewById(R.id.graph);


                    RecyclerView profileRecyclerView = rootView.findViewById(R.id.earning_points);
                    LinearLayoutManager linearLayoutManager
                            = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    profileRecyclerView.setLayoutManager(linearLayoutManager);

                    List<ProfileItem> profileItems = new ArrayList<>();
                    profileItems.add(new ProfileItem("Your Earning History", "Last 9 Earning"));



                    try {

                        JSONArray artistJson = mainJson.getJSONArray("points_array");
                        DataPoint[] dataPointsArray = new DataPoint[artistJson.length()];

                        for (int i = 0; i < artistJson.length(); i++) {

                            JSONObject jsonObject = artistJson.getJSONObject(i);
                            dataPointsArray[i] = new DataPoint(i, jsonObject.getInt("points"));
                            profileItems.add(new ProfileItem(jsonObject.getInt("points")+" Points", jsonObject.getString("date_")));

                        }


                        // first series is a line
                        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPointsArray);
                        //graph.getViewport().setBackgroundColor(Color.argb(255, 0, 0, 222));

                        series.setColor(Color.parseColor("#607D8B"));
                        series.setDrawBackground(true);
                        series.setBackgroundColor(Color.argb(100, 96,125,139));
                        series.setAnimated(true);
                        series.setDrawDataPoints(true);
                        series.setTitle("Points");

                        graph.addSeries(series);


                        graph.getLegendRenderer().setVisible(true);
                        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);


                        ProfileAdapter adapter = new ProfileAdapter(getContext(), profileItems);
                        profileRecyclerView.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetDataFromUrl1 {

        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody formBody) throws IOException {


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }
}
