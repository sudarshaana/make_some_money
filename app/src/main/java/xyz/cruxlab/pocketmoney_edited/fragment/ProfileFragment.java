package xyz.cruxlab.pocketmoney_edited.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.activity.Profile;
import xyz.cruxlab.pocketmoney_edited.classes.ProfileAdapter;
import xyz.cruxlab.pocketmoney_edited.classes.ProfileItem;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.POINTS_FOR_PROFILE;
import static xyz.cruxlab.pocketmoney_edited.C.PROFILE_INFO;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class ProfileFragment extends Fragment {

    RecyclerView profileRecyclerView;
    View rootView;
    LocalData localData;
    TextView failedTextView;
    Context context;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        localData = new LocalData(getContext());
        context = getContext();


        //failedTextView = rootView.findViewById(R.id.failed);

        if (Utils.isNetworkAvailable(getContext())) {
            new LoadDataFromServer().execute();

        } else {
            //failedTextView.setVisibility(View.VISIBLE);
        }

        profileRecyclerView = rootView.findViewById(R.id.recyclerViewProDetails);

        LinearLayoutManager linearLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        profileRecyclerView.setLayoutManager(linearLayoutManager);


        return rootView;
    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getContext(), "Loading Data", "Hold on a second!", true, true);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl1 getData = new GetDataFromUrl1();
            String response = null;
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getContext()))
                        .build();

                response = getData.run(GATE_WAY + PROFILE_INFO, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Log.d("statusResult", result);


            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    List<ProfileItem> profileItems = new ArrayList<>();

                    try {

                        JSONArray artistJson = mainJson.getJSONArray("profile_details");

                        for (int i = 0; i < artistJson.length(); i++) {

                            JSONObject jsonObject = artistJson.getJSONObject(i);

                            //profileItems.add(new ProfileItem(jsonObject.getInt("points")+" Points", jsonObject.getString("date_")));

                            profileItems.add(new ProfileItem("Payment Method", localData.getWithdrawal_method()));
                            profileItems.add(new ProfileItem("Payment Number", localData.getWithDNumber()));

                            profileItems.add(new ProfileItem("Total Withdrawal", jsonObject.getString("total_paid")));
                            profileItems.add(new ProfileItem("Pending Withdrawal", jsonObject.getString("total_pending")));

                            profileItems.add(new ProfileItem("Badge", localData.getBadge()));
                            profileItems.add(new ProfileItem("Total Referral", jsonObject.getString("total_referral")));


                            int status = localData.getAccountStatus();
                            if (status == 0) {
                                profileItems.add(new ProfileItem("Account Status", "Active"));
                            } else if (status == 1) {
                                profileItems.add(new ProfileItem("Account Status", "Temporary Banned"));
                            } else if (status == 2) {
                                profileItems.add(new ProfileItem("Account Status", "Permanently  Banned"));
                            }

                            profileItems.add(new ProfileItem("Account Created", localData.getAccCreated()));


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    ProfileAdapter adapter = new ProfileAdapter(getContext(), profileItems);
                    profileRecyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }

    }

    private class GetDataFromUrl1 {

        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody formBody) throws IOException {


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }
}
