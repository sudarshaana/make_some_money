package xyz.cruxlab.pocketmoney_edited.fragment;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.activity.Referral;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.REFERRAL_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.REF_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_INV_REF_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OWN_REF_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_REF_ALREADY_APPLIED;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class ReferralFragmant extends Fragment {

    RecyclerView dailyPointRecy;
    View rootView;
    LocalData localData;
    TextView failedTextView;


    private TextView referralCode, refCodeAdded;
    RelativeLayout refCodeLayout;
    private String referralCodeString, refCodeSaved;
    private Context context;
    private Button copyRefButton, applyRefCodeButton, shareRefCode;

    private ProgressDialog progressDialog;
    private int statusCode;
    EditText editRefCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_referrral, container, false);

        localData = new LocalData(getContext());
        context = getContext();



        referralCode = rootView.findViewById(R.id.referral);
        referralCodeString = LocalData.getIdentifier(context);
        referralCode.setText(referralCodeString);
        copyRefButton = rootView.findViewById(R.id.copyRefButton);

        refCodeSaved = localData.getRefCode();

       //Toast.makeText(context, refCodeSaved, Toast.LENGTH_SHORT).show();


        refCodeAdded = rootView.findViewById(R.id.refCodeAdded);
        refCodeLayout = (RelativeLayout) rootView.findViewById(R.id.refCodeLayout);

        if (refCodeSaved.equals("")) {
            refCodeAdded.setVisibility(View.GONE);
            refCodeLayout.setVisibility(View.VISIBLE);
        } else {
            refCodeLayout.setVisibility(View.GONE);
            refCodeAdded.setVisibility(View.VISIBLE);
        }

        copyRefButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Referral Code", referralCodeString);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, "Code Copied!", Toast.LENGTH_SHORT).show();

            }
        });
        editRefCode = rootView.findViewById(R.id.editRefCode);
        applyRefCodeButton = rootView.findViewById(R.id.applyRefCodeButton);
        applyRefCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.isNetworkAvailable(context)) {

                    if (!editRefCode.getText().toString().trim().isEmpty()) {
                        new SaveReferralData().execute();
                    } else {
                        editRefCode.setError("Can't be empty.");
                    }

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        shareRefCode = rootView.findViewById(R.id.shareRefButton);
        shareRefCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //tring shareBody = "Here is the share content body";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, referralCodeString);
                startActivity(Intent.createChooser(sharingIntent, "Share Ref Code"));
            }
        });



        
        return rootView;
    }

    private class SaveReferralData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getContext(), "Applying Ref Code", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl1 getData = new GetDataFromUrl1();
            String response = null;
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getContext()))
                        .add(REFERRAL_CODE, editRefCode.getText().toString().trim())
                        .build();

                response = getData.run(GATE_WAY + REF_CODE, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            //Log.d("statusResult", result);

            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);


                    if (statusCode == STATUS_OKAY) {
                        Toast.makeText(context, "Referral Code applied.\nYou've got your points.", Toast.LENGTH_SHORT).show();

                        refCodeAdded.setVisibility(View.VISIBLE);
                        refCodeLayout.setVisibility(View.GONE);

                    } else if (statusCode == STATUS_INV_REF_CODE) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_LONG).show();

                    } else if (statusCode == STATUS_OWN_REF_CODE) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_LONG).show();

                    } else if (statusCode == STATUS_REF_ALREADY_APPLIED) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ERROR) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {
                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    private class GetDataFromUrl1 {

        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody formBody) throws IOException {


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }
}
