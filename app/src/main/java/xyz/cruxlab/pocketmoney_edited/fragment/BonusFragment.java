package xyz.cruxlab.pocketmoney_edited.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;

public class BonusFragment extends Fragment {

    RecyclerView dailyPointRecy;
    View rootView;
    LocalData localData;
    TextView failedTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.bonus_fragment, container, false);
        localData = new LocalData(getContext());

        //failedTextView = rootView.findViewById(R.id.failed);

//        if (Utils.isNetworkAvailable(getContext())) {
//            new LoadDataFromServer().execute();
//        }else{
//            failedTextView.setVisibility(View.VISIBLE);
//        }

        return rootView;
    }
}
