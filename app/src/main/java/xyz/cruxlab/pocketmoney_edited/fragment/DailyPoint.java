package xyz.cruxlab.pocketmoney_edited.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.activity.Leaderboard;
import xyz.cruxlab.pocketmoney_edited.classes.LeaderBoardAdapter;
import xyz.cruxlab.pocketmoney_edited.classes.User;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LEADER_BOARD;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class DailyPoint extends Fragment {

    RecyclerView dailyPointRecy;
    View rootView;
    LocalData localData;
    TextView failedTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.daily_points, container, false);
        localData = new LocalData(getContext());
        failedTextView = rootView.findViewById(R.id.failed);

        if (Utils.isNetworkAvailable(getContext())) {
            new LoadDataFromServer().execute();
        } else {
            failedTextView.setVisibility(View.VISIBLE);
        }

        return rootView;
    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + LEADER_BOARD);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if (result != null) {
                if (!result.isEmpty()) {
                    failedTextView.setVisibility(View.GONE);
                    try {

                        JSONObject mainJson = new JSONObject(result);
                        List<User> userList = new ArrayList<>();


                        try {

                            JSONArray artistJson = mainJson.getJSONArray("daily_points");

                            for (int i = 0; i < artistJson.length(); i++) {

                                JSONObject jsonObject = artistJson.getJSONObject(i);
                                User user = new User();
                                user.setName(jsonObject.getString("name"));
                                user.setPoints(jsonObject.getString("points") + " পয়েন্ট");
                                userList.add(user);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        dailyPointRecy = rootView.findViewById(R.id.dailyPointRecy);
                        LinearLayoutManager linearLayoutManager
                                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                        dailyPointRecy.setLayoutManager(linearLayoutManager);

                        LeaderBoardAdapter adapter = new LeaderBoardAdapter(getContext(), userList);
                        dailyPointRecy.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    Toast.makeText(getContext(), R.string.load_data_failed, Toast.LENGTH_SHORT).show();
                }
            } else {

                Toast.makeText(getContext(), R.string.load_data_failed, Toast.LENGTH_SHORT).show();
            }


        }

    }

    private class GetDataFromUrl {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {

            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getContext()))
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    }

}