package xyz.cruxlab.pocketmoney_edited.classes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import xyz.cruxlab.pocketmoney_edited.R;


public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {
    private Context context;
    private List<ProfileItem> userList;

    public ProfileAdapter(Context context, List<ProfileItem> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_item_style, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final ProfileItem user = userList.get(position);
        holder.title.setText(user.getTitle());
        holder.content.setText(user.getContent());


        ColorGenerator generator = ColorGenerator.MATERIAL;

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(user.getTitle().substring(0, 1), generator.getRandomColor());

        //ImageView image = (ImageView) findViewById(R.id.image_view);
        holder.image.setImageDrawable(drawable);


    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView image;
        private TextView content;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.titleText);
            image = (ImageView) itemView.findViewById(R.id.icon);
            content = (TextView) itemView.findViewById(R.id.contentText);

        }
    }

}
