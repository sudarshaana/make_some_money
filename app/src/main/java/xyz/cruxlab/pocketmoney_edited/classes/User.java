package xyz.cruxlab.pocketmoney_edited.classes;

public class User {

        private String name;
        private String points;

        public User() {
        }

        public User(String name, String points) {
            this.name = name;
            this.points = points;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
}
