package xyz.cruxlab.pocketmoney_edited.classes;

public class ProfileItem {

    String Title;
    String content;

    public ProfileItem() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ProfileItem(String title, String content) {
        Title = title;
        this.content = content;
    }
}
