package xyz.cruxlab.pocketmoney_edited.classes;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import xyz.cruxlab.pocketmoney_edited.R;


public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.ViewHolder> {
    private Context context;
    private List<User> userList;

    public LeaderBoardAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_card_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final User user = userList.get(position);
        holder.name.setText((position + 1) + ". " + user.getName());
        holder.point.setText(user.getPoints());


        ColorGenerator generator = ColorGenerator.MATERIAL;

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(user.getName().substring(0, 1), generator.getRandomColor());

        //ImageView image = (ImageView) findViewById(R.id.image_view);
        holder.image.setImageDrawable(drawable);


    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private ImageView image;
        private TextView point;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.nameText);
            image = (ImageView) itemView.findViewById(R.id.image_view);
            point = (TextView) itemView.findViewById(R.id.pointText);

        }
    }

}
