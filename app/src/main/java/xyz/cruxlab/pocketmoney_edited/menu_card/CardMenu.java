package xyz.cruxlab.pocketmoney_edited.menu_card;

public class CardMenu {
    private int image;
    private String title;
    private int action;

    public CardMenu() {
    }

    public CardMenu(int image, String title, int action) {
        this.image = image;
        this.title = title;
        this.action = action;
    }


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
