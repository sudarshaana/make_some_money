package xyz.cruxlab.pocketmoney_edited.menu_card;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.activity.Earning;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.activity.Leaderboard;
import xyz.cruxlab.pocketmoney_edited.activity.NoticeBoard;
import xyz.cruxlab.pocketmoney_edited.activity.Payment_History;
import xyz.cruxlab.pocketmoney_edited.activity.Profile;
import xyz.cruxlab.pocketmoney_edited.activity.Withdrawals;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    private Context context;
    private List<CardMenu> newsList;

    public CardAdapter(Context context, List<CardMenu> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_style, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final CardMenu cardMenu = newsList.get(position);

        holder.name.setText(cardMenu.getTitle());


        holder.image.setBackgroundResource(cardMenu.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocalData localData = new LocalData(context);

                if (cardMenu.getAction() == 1) {

                    if (localData.getAccountStatus() == 0) { // okay user
                        Intent intent = new Intent(context, Earning.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {
                        Toast.makeText(context, "Your account is banned!!!", Toast.LENGTH_SHORT).show();
                    }

                } else if (cardMenu.getAction() == 2) {
                    // profile
                    Intent intent = new Intent(context, Profile.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

                } else if (cardMenu.getAction() == 3) {

                    if (localData.getAccountStatus() == 0) { // okay user
                        Intent intent = new Intent(context, Withdrawals.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, "Your account is banned!!!", Toast.LENGTH_SHORT).show();
                    }


                } else if (cardMenu.getAction() == 4) {
                    //about
                    Intent intent = new Intent(context, Leaderboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else if (cardMenu.getAction() == 5) {
                    //about
                    Intent intent = new Intent(context, Payment_History.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
//                else if (cardMenu.getAction() == 5) {
//                    //tutorial
//
//                    Intent intent = new Intent(context, Tutorial.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
//
//                } else if (cardMenu.getAction() == 6) {
//                    //about
//                    Intent intent = new Intent(context, About.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
//                }
                else if (cardMenu.getAction() == 6) {
                    //about
                    Intent intent = new Intent(context, NoticeBoard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);

        }
    }

}
