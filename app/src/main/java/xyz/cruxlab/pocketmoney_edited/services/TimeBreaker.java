package xyz.cruxlab.pocketmoney_edited.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

public class TimeBreaker extends Service {

    public static boolean timerRunning = false;

    private final static String TAG = "BroadcastService";

    public static final String COUNTDOWN_BR = "your_package_name.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);

    CountDownTimer cdt = null;


    public TimeBreaker() {
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Log.d("timer_started", "started");
        timerRunning = true;

        cdt = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                //Log.i(TAG, "Countdown seconds remaining: " + millisUntilFinished / 1000);
                bi.putExtra("countdown", millisUntilFinished);
                sendBroadcast(bi);
                //Log.d("timer_running", " " + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {
                //Log.i(TAG, "Timer finished");
                timerRunning = false;
                //Log.d("timer_finished", "timer_finished");
            }
        };

        cdt.start();

    }

    @Override
    public void onDestroy() {

        cdt.cancel();
        //Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid) {

        return super.onStartCommand(intent, flags, startid);
    }


}
