package xyz.cruxlab.pocketmoney_edited.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;

public class TimeTracker extends Service {

    long EndTime = 30000;  //millisecond
    public static boolean timerEnd = false;


    public TimeTracker() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        // Code to execute when the service is first created
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid) {

//        Random r = new Random();
//        EndTime = r.nextInt(36000 - 25000) + 25000;

        timerEnd = false;
        new CountDownTimer(EndTime, 1000) {

            public void onTick(long millisUntilFinished) {
                //Toast.makeText(TimeTracker.this, (long) (millisUntilFinished / 1000)+"", Toast.LENGTH_SHORT).show();
                //long secondUntilFinished = (long) (millisUntilFinished / 1000);

                //Log.d("timer", "running " + secondUntilFinished);
            }

            public void onFinish() {

                timerEnd = true;
                //Toast.makeText(TimeTracker.this, "Times up", Toast.LENGTH_SHORT).show();
                //Log.d("timer", "finished");
            }
        }.start();

        return START_STICKY;
    }


}
