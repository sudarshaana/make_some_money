package xyz.cruxlab.pocketmoney_edited.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

public class BroadcastService extends Service {

    //private final static String TAG = "adsRemain";

    public static final String COUNTDOWN_BR = "your_package_name.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);

    CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    public void onDestroy() {

        cdt.cancel();
        //Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Log.i(TAG, "Starting timer...");

        int sec = 29*30;
        if (intent != null) {
             sec = intent.getIntExtra("time", 0);
        }
        //Log.d("adsRemain timer", sec +"");



        cdt = new CountDownTimer(sec * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                //Log.i(TAG, "Countdown seconds remaining: " + millisUntilFinished / 1000);
                bi.putExtra("countdown", millisUntilFinished);
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                //Log.i(TAG, "Timer finished");
            }
        };

        cdt.start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}