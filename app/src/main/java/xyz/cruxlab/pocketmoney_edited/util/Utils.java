package xyz.cruxlab.pocketmoney_edited.util;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

public class Utils {


//    public static String getJSONString(String url, String key) {
//        String jsonString = null;
//        HttpURLConnection linkConnection = null;
//        //List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//
//
//        try {
//            //	linkConnection.setRequestMethod("POST");
//
//
//            URL linkurl = new URL(url);
//            linkConnection = (HttpURLConnection) linkurl.openConnection();
//            linkConnection.setRequestMethod("POST");
//            linkConnection.setRequestProperty("api-key", key);
//            linkConnection.setDoOutput(true);
//            int responseCode = linkConnection.getResponseCode();
//
//            if (responseCode == HttpURLConnection.HTTP_OK) {
//                InputStream linkinStream = linkConnection.getInputStream();
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//                int j = 0;
//                while ((j = linkinStream.read()) != -1) {
//                    baos.write(j);
//                }
//                byte[] data = baos.toByteArray();
//                jsonString = new String(data);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (linkConnection != null) {
//                linkConnection.disconnect();
//            }
//        }
//        return jsonString;
//    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String secToMinSec(long totalSecs) {
        totalSecs = Math.abs(totalSecs);
        int minutes = (int) ((totalSecs % 3600) / 60);
        int seconds = (int) (totalSecs % 60);

        return minutes + " মি. " + seconds + " সে.";
    }


    public static int toHundred(int num) {
        return (num - (num%10));
    }
}
