package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.ads.LoadService;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.CK_POINT;
import static xyz.cruxlab.pocketmoney_edited.C.CONVERSION_RATE;
import static xyz.cruxlab.pocketmoney_edited.C.CURRENT_POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.ERROR_PAYMENT_LIMIT;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.PIN;
import static xyz.cruxlab.pocketmoney_edited.C.POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.RECENT_WITHDRAWAL;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_NEW_ACCOUNT;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_NOT_ENOUGH_POINT;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_NO_Point;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_PIN_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.TOTAL_POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAWAL_REQ;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAW_NO;

public class Withdrawals extends AppCompatActivity {

    private Context context;
    private ProgressDialog progressDialog;
    private int statusCode;
    LocalData localData;

    TextView currentPoint, totalPoint, conversionRate, totalBalance, recentWithdrawal, maxWithdrawalText;
    String currentPointString = "0", totalPointString = "0", conversionRateString = "0", totalBalanceString = "0";
    Button withdrawalButton;
    float rate;
    int pointToTaka, inputTakaInt, requestedPoint;

    EditText takaEditText;
    String withdrawalNumber, pin;
    private FirebaseAnalytics mFirebaseAnalytics;
    int accountAge = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawals);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = getApplicationContext();
        localData = new LocalData(context);

        currentPoint = findViewById(R.id.currentBalance);
        totalPoint = findViewById(R.id.totalPoints);
        conversionRate = findViewById(R.id.conversionRate);
        totalBalance = findViewById(R.id.taka);
        recentWithdrawal = findViewById(R.id.recentWithdrawal);
        maxWithdrawalText = findViewById(R.id.maxWithdrawal);
        //edittext
        takaEditText = findViewById(R.id.withdrawalEdittext);
        //LoadService.LoadAds(context);


        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Points/withdrawal");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        if (Utils.isNetworkAvailable(context)) {
            new LoadDataFromServer().execute();

        } else {
            Toast.makeText(context, "ইন্টারনেট কানেকশন নেই।", Toast.LENGTH_SHORT).show();
        }


        withdrawalButton = findViewById(R.id.withdrawalButton);
        withdrawalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //int cB = Integer.parseInt(currentPointString);
                // Log.d("taka", Utils.toHundred(inputTakaInt) + "");

                if (!takaEditText.getText().toString().isEmpty()) {
                    inputTakaInt = Integer.parseInt(takaEditText.getText().toString());

                    if (pointToTaka > 199) {

                        if (inputTakaInt > 199) {
                            //String inputTaka = takaEditText.getText().toString();

                            withdrawalNumber = localData.getWithDNumber();

                            if (withdrawalNumber.length() < 5) {
                                withdrawalNoti(5);

                            } else {

                                if (inputTakaInt != Utils.toHundred(inputTakaInt)) {
                                    withdrawalNoti(4);
                                } else{

                                    if (inputTakaInt > pointToTaka) {

                                        Toast.makeText(context, "টাকার পরিমান সঠিক নয়। সর্বোচ্চঃ" + pointToTaka, Toast.LENGTH_SHORT).show();
                                    } else {
                                        //Toast.makeText(context, "okay", Toast.LENGTH_SHORT).show();
                                        confirmWithdrawal();
                                    }

                                }



                            }

                        } else {
                            withdrawalNoti(1);
                        }


                    } else {
                        withdrawalNoti(1);
                    }
                } else {
                    takaEditText.setError("Error!");
                }


            }
        });

    }


    private void withdrawalNoti(int status) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_red));
        title.setTextColor(getResources().getColor(R.color.white));

        if (status == 1)// low balance
        {
            title.setText("লো ব্যালেন্স");
            content.setText(Html.fromHtml("আপনার অ্যাকাউন্টে <b>পর্যাপ্ত পয়েন্ট নেই</b>। <br>মিনিমাম ২০০ টাকা অর্জন করে চেষ্টা করুন।"));

        } else if (status == 2) // acc date create
        {
            //LocalData localData = new LocalData(context);

            title.setText("নতুন অ্যাকাউন্ট");
            content.setText(Html.fromHtml("নতুন অ্যাকাউন্টের মিনিমাম বয়স <b>পনের (১৫) দিন</b> হলে টাকা তোলার রিকোয়েস্ট করতে পারবেন। আপনার আকাউন্টের বয়সঃ " + accountAge));

        } else if (status == 3) // pin error
        {
            title.setText("পিন ইরর");
            content.setText(Html.fromHtml("আপনি যে পিন দিয়েছেন তা ঠিক নয়। সঠিক পিন দিন। <br>পিন ভুলে গেলে প্রোফাইল থেকে Forgot Pin দিন"));

        }
        else if (status == 4) //error tk digit
        {
            title.setText("ইনভ্যালিড ইনপুট");
            content.setText(Html.fromHtml("আপনি 200, 210, ... 250 টাকা তুলতে পারবেন। <br>যেমনঃ 208 টাকা থাকলে 200 টাকার রিকোয়েস্ট করুন। "));
        }
        else if (status == 5) //no withdrawal number
        {
            title.setText("উইথড্রল নম্বর নেই");
            content.setText(Html.fromHtml("টাকা পাঠানোর নম্বর পাওয়া যায় নি। প্রোফাইল থেকে উইথড্রল (Withdrawal) নম্বর যোগ করে চেষ্টা করুন।"));
        }else if (status == 6) //no withdrawal number
        {
            title.setText("Payment Limit Exceed!");
            content.setText(Html.fromHtml("You've already 2 pending Payments. Request after the payment."));
        }


        Button button = (Button) myDialogue.findViewById(R.id.button);
        button.setText("ওকে");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();

            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();
    }


    private void confirmWithdrawal() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.confirm_withdrawal, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        TextView content = (TextView) myDialogue.findViewById(R.id.message);
        final EditText pinET = (EditText) myDialogue.findViewById(R.id.pinET);


        requestedPoint = (int) (Math.ceil(inputTakaInt / rate));

        title.setText("কনফর্ম করুন");

        content.setText(Html.fromHtml("আপনি <b>" + inputTakaInt + "</b> টাকা (" + requestedPoint + " পয়েন্ট) উত্তোলন করতে যাচ্ছেন। <br>পেমেন্ট নম্বরঃ <b>" + localData.getWithDNumber() + "</b> MAX Limit: 2 times"));


        Button okay = (Button) myDialogue.findViewById(R.id.okay);
        Button cancel = (Button) myDialogue.findViewById(R.id.cancel);

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //alertDialogue.dismiss();
                if (pinET.getText().toString().length() > 0) {
                    pin = pinET.getText().toString();

                    if (Utils.isNetworkAvailable(context)) {
                        alertDialogue.dismiss();
                        new WithdrawalRequest().execute();
                    }

                } else {
                    Toast.makeText(context, "পিন ইনপুট দিন।", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();
            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();
    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Withdrawals.this, "Checking...", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                //Log.d("status", localData.getUserID() + " -- " + LocalData.getIdentifier(getApplicationContext()));
                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                        .build();


                response = getData.run(GATE_WAY + CK_POINT, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Log.d("status", result);
            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);

                    if (statusCode == STATUS_OKAY) {

                        currentPointString = mainJson.getString(CURRENT_POINTS);
                        totalPointString = mainJson.getString(TOTAL_POINTS);
                        conversionRateString = mainJson.getString(CONVERSION_RATE);
                        if (!conversionRateString.isEmpty()) {
                            rate = Float.parseFloat(conversionRateString);
                        }

                        //Log.d("status", currentPointString + " " + totalPointString + " " + rate + " " + (Float.parseFloat(currentPointString) * rate));

                        currentPoint.setText("Current Points: " + currentPointString);
                        totalPoint.setText("Total Points: " + totalPointString);
                        conversionRate.setText("Current Conversion Rate: " + conversionRateString);

                        pointToTaka = (int) (Float.parseFloat(currentPointString) * rate);

                        totalBalance.setText("Points into Taka: " + pointToTaka + " tk*");
                        maxWithdrawalText.setText("সর্বোচ্চ তুলতে পারবেনঃ " + pointToTaka + " টাকা");

                        takaEditText.setText(Utils.toHundred(pointToTaka) + "");

                        //Log.d("status", pointToTaka + " " + Utils.toHundred(pointToTaka));

                        recentWithdrawal.setText(mainJson.getString(RECENT_WITHDRAWAL));


                    } else if (statusCode == STATUS_NO_Point) {
                        // status -1 == error
                        Toast.makeText(context, "No point found.", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ERROR) {
                        // status -1 == error
                        Toast.makeText(context, "Error occurred. Try Again.", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {

                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    // request for withdrawal
    private class WithdrawalRequest extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Withdrawals.this, "Requesting...", "অপেক্ষা করুন", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                //Log.d("status", localData.getUserID() + " -- " + LocalData.getIdentifier(getApplicationContext()));
                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                        .add(POINTS, requestedPoint + "")
                        .add(PIN, pin)
                        .add(WITHDRAW_NO, localData.getWithDNumber())
                        .build();


                response = getData.run(GATE_WAY + WITHDRAWAL_REQ, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Log.d("status", result);

            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);


                    //Log.d("status", statusCode + "");
                    //Log.d("status", userID + "");

                    if (statusCode == STATUS_OKAY) {

                        Toast.makeText(context, "আপনার রিকোয়েস্টটি সফল ভাবে জমা হয়েছে।", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(context, Withdrawals.class));
                        finish();

                    }
                    else if (statusCode == ERROR_PAYMENT_LIMIT) {
                        withdrawalNoti(6);

                    }
                    else if (statusCode == STATUS_PIN_ERROR) {
                        withdrawalNoti(3);

                    } else if (statusCode == STATUS_NEW_ACCOUNT) {
                        accountAge = mainJson.getInt("msg");
                        withdrawalNoti(2);

                    } else if (statusCode == STATUS_NOT_ENOUGH_POINT) {
                        withdrawalNoti(1);

                    } else if (statusCode == STATUS_NO_Point) {
                        Toast.makeText(context, "No point found.", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ERROR) {
                        Toast.makeText(context, "Error occurred. Try Again.", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {
                        //Alert.accountError(context);
                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }


    private class GetDataFromUrl {

        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody formBody) throws IOException {


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }

}
