package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.classes.LeaderBoardAdapter;
import xyz.cruxlab.pocketmoney_edited.classes.User;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LEADER_BOARD;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAWAL_HIST;

public class Payment_History extends AppCompatActivity {


    LocalData localData;
    TextView userDetails;
    private ProgressDialog pdia;
    RecyclerView dailyPointRecy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment__history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        localData = new LocalData(getApplicationContext());
        userDetails = findViewById(R.id.userText);


        if (Utils.isNetworkAvailable(getApplicationContext())) {

            new LoadDataFromServer().execute();
        } else {
            Toast.makeText(this, "ইন্টারনেট সংযোগ নেই।", Toast.LENGTH_SHORT).show();
        }


    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdia = new ProgressDialog(Payment_History.this);
            pdia.setMessage("Loading ...");
            pdia.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + WITHDRAWAL_HIST);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                if (!result.isEmpty()) {
                    try {


                        JSONObject mainJson = new JSONObject(result);

                        JSONArray jsonArray = mainJson.getJSONArray("hist");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            userDetails.setText("মোট পেইডঃ " + jsonObject.getString("paid") +
                                    " টি\nপ্রোসেসিংঃ " + jsonObject.getString("pending") + " টি");

                        }


                        List<User> userList = new ArrayList<>();

                        JSONArray artistJson = mainJson.getJSONArray("withdrawal_req");

                        for (int i = 0; i < artistJson.length(); i++) {
                            JSONObject jsonObject = artistJson.getJSONObject(i);
                            User user = new User();
                            user.setName(jsonObject.getString("name"));
                            user.setPoints(jsonObject.getString("balance_tk") + " tk by " +
                                    jsonObject.getString("withdrawl_method") +
                                    " (" + jsonObject.getString("status") + ") ");
                            userList.add(user);

                        }

                        dailyPointRecy = findViewById(R.id.dailyPointRecy);
                        LinearLayoutManager linearLayoutManager
                                = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        dailyPointRecy.setLayoutManager(linearLayoutManager);

                        LeaderBoardAdapter adapter = new LeaderBoardAdapter(getApplicationContext(), userList);
                        dailyPointRecy.setAdapter(adapter);


                        if (pdia.isShowing() && pdia != null)
                            pdia.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(Payment_History.this, R.string.load_data_failed, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(Payment_History.this, R.string.load_data_failed, Toast.LENGTH_SHORT).show();
            }


        }

    }

    private class GetDataFromUrl {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {

            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    }

}
