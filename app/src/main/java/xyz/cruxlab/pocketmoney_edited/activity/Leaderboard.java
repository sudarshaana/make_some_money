package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.fragment.DailyPoint;
import xyz.cruxlab.pocketmoney_edited.fragment.CurrentPoints;
import xyz.cruxlab.pocketmoney_edited.fragment.TotalPointsFragment;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LEADER_BOARD;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class Leaderboard extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    LocalData localData;
    TextView userDetails;
    private ProgressDialog pdia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


        localData = new LocalData(getApplicationContext());
        userDetails = findViewById(R.id.userText);

        if (Utils.isNetworkAvailable(getApplicationContext())) {

            new LoadDataFromServer().execute();
        } else {
            Toast.makeText(this, "ইন্টারনেট সংযোগ নেই।", Toast.LENGTH_SHORT).show();
        }


    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {

                case 0:
                    return new DailyPoint();

                case 1:
                    return new CurrentPoints();

                case 2:
                    return new TotalPointsFragment();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdia = new ProgressDialog(Leaderboard.this);
            pdia.setMessage("Loading ...");
            pdia.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + LEADER_BOARD);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                if (!result.isEmpty()) {
                    try {

                        JSONObject mainJson = new JSONObject(result);

                        JSONArray jsonArray = mainJson.getJSONArray("user");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            userDetails.setText(jsonObject.getString("name") + "\nToday: " + jsonObject.getString("daily_point") + " point Total: " + jsonObject.getString("current_point") + " point");

                        }

                        if (pdia.isShowing() && pdia != null)
                            pdia.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(Leaderboard.this, R.string.load_data_failed, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(Leaderboard.this, R.string.load_data_failed, Toast.LENGTH_SHORT).show();
            }


        }

    }

    private class GetDataFromUrl {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {

            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    }
}
