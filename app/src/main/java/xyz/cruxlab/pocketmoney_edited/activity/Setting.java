package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.ads.LoadService;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.firebase.SharedPrefManager;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.CK_PROFILE;
import static xyz.cruxlab.pocketmoney_edited.C.FIREBASE_ID;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.NAME;
import static xyz.cruxlab.pocketmoney_edited.C.NUMBER_WD;
import static xyz.cruxlab.pocketmoney_edited.C.PIN;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OLD_USER;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.UPDATE_PROFILE;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAW_METHOD;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAW_NO;

public class Setting extends AppCompatActivity {

    Context context;
    private ProgressDialog progressDialog;
    private int statusCode;
    LocalData localData;
    private EditText nameEditText, withDrawlNumberET, oldPin, newPin;
    private Spinner withDrawlMethodSpnner;
    private String name, pinString = "", oldPinString, newPinString, withdrawalNumber, withdrawalMethod;
    Button updateProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = getApplicationContext();
        localData = new LocalData(context);

        //LoadService.LoadAds(context);
        // var init
        nameEditText = findViewById(R.id.nameEditText);
        withDrawlNumberET = findViewById(R.id.wt_numberEditText);
        withDrawlMethodSpnner = findViewById(R.id.wt_method_spinner);
        //

        oldPin = findViewById(R.id.oldPin);
        newPin = findViewById(R.id.newPin);

        if (Utils.isNetworkAvailable(context)) {
            new LoadDataFromServer().execute();

        } else {
            Toast.makeText(context, "ইন্টারনেট কানেকশন নেই।", Toast.LENGTH_SHORT).show();
        }

        updateProfile = findViewById(R.id.saveProfileButton);
        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = nameEditText.getText().toString();
                withdrawalNumber = withDrawlNumberET.getText().toString();
                withdrawalMethod = withDrawlMethodSpnner.getSelectedItem().toString();

                oldPinString = oldPin.getText().toString();
                newPinString = newPin.getText().toString();

                if (!isNull(oldPinString) || !isNull(newPinString)) {
                    // pin may change
                    if (isNull(newPinString)) {
                        Toast.makeText(context, "নতুন ও পুরাতন পিন দিন", Toast.LENGTH_SHORT).show();
                    } else {
                        if (pinString.contentEquals(oldPinString)) {
                            pinString = newPinString;

                            // time to update
                            if (isNull(name) || isNull(withdrawalNumber) || isNull(withdrawalNumber)) {
                                Toast.makeText(context, "সঠিক ভাবে পুরন করুন।", Toast.LENGTH_SHORT).show();

                            } else if (pinString == null || pinString.isEmpty()) {
                                Toast.makeText(context, "ইরর!\nআবার চেস্টা করুন।", Toast.LENGTH_SHORT).show();
                            } else {
                                new UpdateInfo().execute();
                            }


                        } else {
                            Toast.makeText(context, "পুরাতন পিন সঠিক নয়", Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    // pin is not changing
                    if (isNull(name) || isNull(withdrawalNumber) || isNull(withdrawalNumber)) {

                        Toast.makeText(context, "সঠিক ভাবে পুরন করুন।", Toast.LENGTH_SHORT).show();

                    } else {
                        new UpdateInfo().execute();
                    }
                }

            }
        });
    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Setting.this, "প্রোফাইল লোড হচ্ছে...", "একটু অপেক্ষা করুন!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                        .build();


                //Log.d("status", localData.getUserID() + " " + LocalData.getIdentifier(getApplicationContext()));


                response = getData.run(GATE_WAY + CK_PROFILE, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Log.d("status", result);
            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);


                    //Log.d("status", statusCode + "");
                    //Log.d("status", userID + "");

                    if (statusCode == STATUS_OKAY) {

                        pinString = mainJson.getString(PIN);

                        name = mainJson.getString(NAME);
                        if (!name.isEmpty())
                            nameEditText.setText(name);

                        withdrawalNumber = mainJson.getString(NUMBER_WD);
                        if (!withdrawalNumber.isEmpty())
                            withDrawlNumberET.setText(mainJson.getString(NUMBER_WD));
                        //Log.d("status", mainJson.getString(withdrawalNumber) + " fgf");

                        withdrawalMethod = mainJson.getString(WITHDRAW_METHOD);
                        if (!withdrawalMethod.isEmpty())
                            setSpinnerValue(withdrawalMethod);


                    } else if (statusCode == STATUS_OLD_USER) {


                    } else if (statusCode == STATUS_ERROR) {
                        // status -1 == error
                        Toast.makeText(context, "ইরর!\nআবার চেস্টা করুন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {
                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        private void setSpinnerValue(String compareValue) {

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.wi_method, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            withDrawlMethodSpnner.setAdapter(adapter);
            if (compareValue != null) {
                int spinnerPosition = adapter.getPosition(compareValue);
                withDrawlMethodSpnner.setSelection(spinnerPosition);
            }
        }

    }

    private class GetDataFromUrl {

        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody formBody) throws IOException {


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }

    private class UpdateInfo extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Setting.this, "আপডেট হচ্ছে...", "একটু অপেক্ষা করুন!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {

                RequestBody formBody = new FormBody.Builder()
                        .add(USER_ID, localData.getUserID() + "")
                        .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                        .add(NAME, name)
                        .add(PIN, pinString)
                        .add(FIREBASE_ID, SharedPrefManager.getInstance(context).getDeviceToken())
                        .add(WITHDRAW_METHOD, withdrawalMethod)
                        .add(WITHDRAW_NO, withdrawalNumber)
                        .build();


                // Log.d("status", name + " " + pinString + " " + withdrawalMethod + " " + withdrawalNumber);

                response = getData.run(GATE_WAY + UPDATE_PROFILE, formBody);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //Log.d("status", result);
            if (result != null) {


                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);


                    //Log.d("status", statusCode + "");
                    //Log.d("status", userID + "");

                    if (statusCode == STATUS_OKAY) {

                        Toast.makeText(context, R.string.update_successfully, Toast.LENGTH_SHORT).show();

                        localData.setPin(mainJson.getString(PIN));
                        localData.setWithDNumber(mainJson.getString(WITHDRAW_NO));

                        localData.setName(mainJson.getString(NAME));
                        localData.setWithdrawal_method(mainJson.getString(WITHDRAW_METHOD));

//                        Intent intent = new Intent(context, Setting.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
                        finish();

                    } else if (statusCode == STATUS_OLD_USER) {

                    } else if (statusCode == STATUS_ERROR) {
                        // status -1 == error
                        Toast.makeText(context, R.string.error_try_again, Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {
                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

//        private void setSpinnerValue(String compareValue) {
//
//            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.wi_method, android.R.layout.simple_spinner_item);
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            withDrawlMethodSpnner.setAdapter(adapter);
//            if (compareValue != null) {
//                int spinnerPosition = adapter.getPosition(compareValue);
//                withDrawlMethodSpnner.setSelection(spinnerPosition);
//            }
//        }

    }

    public static boolean isNull(String s) {
        // Null-safe, short-circuit evaluation.
        return s == null || s.trim().isEmpty();
    }
}
