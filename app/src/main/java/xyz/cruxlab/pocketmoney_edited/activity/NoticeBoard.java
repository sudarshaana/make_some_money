package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.classes.Notice;
import xyz.cruxlab.pocketmoney_edited.classes.NoticeAdapter;
import xyz.cruxlab.pocketmoney_edited.classes.User;
import xyz.cruxlab.pocketmoney_edited.menu_card.CardAdapter;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LEADER_BOARD;
import static xyz.cruxlab.pocketmoney_edited.C.NOTICE_LIST;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class NoticeBoard extends AppCompatActivity {

    Context context;
    private ProgressDialog progressDialog;
    private List<Notice> noticeList = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = getApplicationContext();


        if (Utils.isNetworkAvailable(context)) {

            new LoadDataFromServer().execute();

        } else {
            Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_SHORT).show();
        }

    }

    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(NoticeBoard.this, "Loading data", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + NOTICE_LIST);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (NoticeBoard.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                    return;
                }
            }

            if (result != null){

                if (!result.isEmpty()) {
                    //failedTextView.setVisibility(View.GONE);
                    try {

                        JSONObject mainJson = new JSONObject(result);



                        try {

                            JSONArray artistJson = mainJson.getJSONArray("notice_list");

                            for (int i = 0; i < artistJson.length(); i++) {

                                JSONObject jsonObject = artistJson.getJSONObject(i);

                                Notice notice = new Notice();
                                notice.setId(jsonObject.getInt("id"));
                                notice.setTitle(jsonObject.getString("title"));
                                notice.setCategory(jsonObject.getString("category"));
                                notice.setDate(jsonObject.getString("date_time"));
                                noticeList.add(notice);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        RecyclerView recyclerView = findViewById(R.id.noticeRecycler);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(mLayoutManager);

                        NoticeAdapter adapter = new NoticeAdapter(context, noticeList);
                        recyclerView.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    Toast.makeText(context, R.string.load_data_failed, Toast.LENGTH_SHORT).show();
                }
            }



            if (!NoticeBoard.this.isFinishing() && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    private class GetDataFromUrl {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {

            RequestBody formBody = new FormBody.Builder()
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notice_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.reload_notice) {


            if (Utils.isNetworkAvailable(context)) {
                if (noticeList.size() > 0) {
                    noticeList.clear();
                }

                new LoadDataFromServer().execute();

            } else {
                Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_SHORT).show();
            }


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
