package xyz.cruxlab.pocketmoney_edited.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.jaredrummler.android.device.DeviceName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.DEVICE_NAME;
import static xyz.cruxlab.pocketmoney_edited.C.ERROR_NEW_USER_LIMIT;
import static xyz.cruxlab.pocketmoney_edited.C.ERROR_TOKEN_UPDATE;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LOGIN;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.PHONE_NO;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OLD_USER;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.UPDATE_TOKEN;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class LoginActivityGmail extends BaseActivity implements
        View.OnClickListener {


    private static final int RC_SIGN_IN = 9001;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private GoogleSignInClient mGoogleSignInClient;
    private TextView mStatusTextView;
    private TextView mDetailTextView;

    LocalData localData;
    Context context;
    private ImageView coin;


    private int statusCode;
    private int userID;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_gmail);


        coin = findViewById(R.id.coin_icon);
        rotationFirst();


        context = getApplicationContext();
        localData = new LocalData(context);


        // Views
        mStatusTextView = findViewById(R.id.status);
        mDetailTextView = findViewById(R.id.detail);


        // Button listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        //findViewById(R.id.problem).setOnClickListener(this);

        // [START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.api))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]


        switch (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)) {
            case ConnectionResult.SERVICE_MISSING:
                GoogleApiAvailability.getInstance().getErrorDialog(this, ConnectionResult.SERVICE_MISSING, 0).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                GoogleApiAvailability.getInstance().getErrorDialog(this, ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED, 0).show();
                break;
            case ConnectionResult.SERVICE_DISABLED:
                GoogleApiAvailability.getInstance().getErrorDialog(this, ConnectionResult.SERVICE_DISABLED, 0).show();
                break;
        }


    }

//    private void signinStart() {
//
//
//    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void rotationFirst() {
        coin.animate().withLayer()
                .rotationY(90)
                .setDuration(400)
                .withEndAction(
                        new Runnable() {
                            @Override
                            public void run() {
                                rotate();

                            }
                        }
                ).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void rotate() {

        coin.setRotationY(-90);
        coin.animate().withLayer()
                .rotationY(0)
                .setDuration(400)
                .withEndAction(
                        new Runnable() {
                            @Override
                            public void run() {
                                rotationFirst();

                            }
                        }
                )
                .start();
    }


    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_check_user]

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                //Log.w(TAG, "Google sign in failed", e);
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        // [START_EXCLUDE silent]
        showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            // Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = mAuth.getCurrentUser();


                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]

    // [START signin]
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

//    private void signOut() {
//        // Firebase sign out
//        mAuth.signOut();
//
//        // Google sign out
//        mGoogleSignInClient.signOut().addOnCompleteListener(this,
//                new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        updateUI(null);
//                    }
//                });
//    }


    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {

            findViewById(R.id.sign_in_button).setVisibility(View.GONE);


            // saving data
            localData.setName(user.getDisplayName());
            localData.setGmail(user.getEmail());
            localData.setPhotoUrl(user.getPhotoUrl() + "");


            if (Utils.isNetworkAvailable(getApplicationContext())) {
                new LoadDataFromServer().execute();
            } else
                Toast.makeText(context, "ইন্টারনেট কানেকশন নেই।", Toast.LENGTH_SHORT).show();


            // starting activity
//            Intent intent = new Intent(context, ProfileSetUp.class);
//            startActivity(intent);
//            finish();


        } else {
            mStatusTextView.setText(R.string.signed_out);
            mDetailTextView.setText(null);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.sign_in_button) {
            signIn();
        }
//        if (i == R.id.problem) {
//            startActivity(new Intent(context, LoginProblemSolved.class));
//        }
    }


    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(ProfileSetUp.this, "Checking...", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + LOGIN);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {

                //Log.d("Response", result);

                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);

                    if (statusCode == STATUS_OKAY) {

                        userID = mainJson.getInt(USER_ID);
                        localData.setUserID(userID);
                        //localData.setLoggedIn(true);
                        Toast.makeText(context, "আপনি ১০০ পয়েন্ট বোনাস পেয়েছেন", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(context, ProfileSetUp.class);
                        startActivity(intent);
                        finish();


                    } else if (statusCode == STATUS_OLD_USER) {


                        userID = mainJson.getInt(USER_ID);
                        localData.setUserID(userID);
                        localData.setLoggedIn(true);

                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    else if (statusCode == ERROR_NEW_USER_LIMIT) {

                        Toast.makeText(context, "আজকে নতুন ইউজার রেজিস্ট্রেশনের লিমিট শেষ!\nরাত ১২ টার পরে আবার চেস্টা করুন ", Toast.LENGTH_LONG).show();
                        FirebaseAuth.getInstance().signOut();


                    } else if (statusCode == STATUS_ERROR) {

                        // status -1 == error
                        Toast.makeText(context, "Error occurred. Try Again.", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {

                        userID = mainJson.getInt(USER_ID);
                        localData.setUserID(userID);
                        LocalData localData = new LocalData(getApplicationContext());
                        localData.setLoggedIn(false);

                        showErrorToken(Integer.parseInt(mainJson.getString("msg")));

                        //Log.d("MessageResponse", mainJson.getString("msg"));


                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetDataFromUrl {

        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {

            RequestBody formBody = new FormBody.Builder()
                    .add(PHONE_NO, localData.getGmail())
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .add(DEVICE_NAME, android.os.Build.MODEL + " - " + Build.BRAND)
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }


    private void showErrorToken(int leftTry) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.token_dialogue, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("Error With Account");

        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_red));
        title.setTextColor(getResources().getColor(R.color.white));

        Button button = (Button) myDialogue.findViewById(R.id.button);
        Button button1 = (Button) myDialogue.findViewById(R.id.button1);

        if (leftTry > 0) {
            content.setText(Html.fromHtml("আপনার ডিভাইস ভেরিফাই করা যায় নি। <br>বর্তমান ডিভাইস আপডেট করে নেবেন?<br>সুযোগ আছে : " + leftTry + "টি<br><br><small>[একটি ডিভাইসে একটি মাত্র আকাউন্ট ব্যবহার করবেন]</small>"));

            button.setText("Request Now");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogue.dismiss();

                    if (Utils.isNetworkAvailable(context)) {
                        new UpdateToken().execute();
                    } else {
                        Toast.makeText(context, "ইন্টারনেট সংযোগ নেই। পয়েন্ট যোগ করা যায়নি।", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            button1.setText("Cancel");
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogue.dismiss();
                    FirebaseAuth.getInstance().signOut();

                }
            });

        } else {
            content.setText(Html.fromHtml("আপনার অটো ডিভাইস আপডেট লিমিট শেষ। মেইল ও টোকেন সহ FB গ্রুপে পোস্ট দিন\n\n<small>একটি ডিভাইসে একটি মাত্র আকাউন্ট ব্যবহার করবেন।</small>"));

            button.setText("Copy Token");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FirebaseAuth.getInstance().signOut();
                    // copy token
                    String referralCodeString = LocalData.getIdentifier(getApplicationContext());
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Token Code", referralCodeString);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getApplicationContext(), "Code Copied!", Toast.LENGTH_SHORT).show();
                }
            });

            button1.setText("FB Group Link");
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FirebaseAuth.getInstance().signOut();

                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/2149734491977371")));
                    } catch (android.content.ActivityNotFoundException ignored) {
                        Toast.makeText(context, "No browser found!", Toast.LENGTH_SHORT).show();
                    }
                    alertDialogue.dismiss();

                }
            });


        }


        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.setCancelable(false);
        alertDialogue.show();
    }

    private class UpdateToken extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(ProfileSetUp.this, "Checking...", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl1 getData = new GetDataFromUrl1();
            String response = null;
            try {
                response = getData.run(GATE_WAY + UPDATE_TOKEN);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {

                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);

                    if (statusCode == STATUS_OKAY) {

//                        userID = mainJson.getInt(USER_ID);
//                        localData.setUserID(userID);
//
                        //localData.setLoggedIn(true);

                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (statusCode == ERROR_TOKEN_UPDATE) {

                        showErrorToken(-1);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetDataFromUrl1 {

        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {


            //Log.d("status -", PhoneNo_string + " " + LocalData.getIdentifier(getApplicationContext()));

            RequestBody formBody = new FormBody.Builder()
                    .add(PHONE_NO, localData.getGmail())
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .add(USER_ID, userID + "")
                    .add(DEVICE_NAME, android.os.Build.MODEL + " - " + Build.BRAND)
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }
}



