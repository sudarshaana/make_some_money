package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.classes.Notice;

import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.NOTICE_DETAILS;

public class NoticeDetails extends AppCompatActivity {


    WebView webView;
    ProgressDialog pd;
    String urlString;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id = getIntent().getStringExtra("id");

        urlString = GATE_WAY + NOTICE_DETAILS + "?id=" + id;

        webView = findViewById(R.id.webView);
        load(urlString);


        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String sTitle) {
                super.onReceivedTitle(view, sTitle);

                if (sTitle.equals("Webpage not available"))
                    getSupportActionBar().setTitle("Can't load Notice");
                else
                    getSupportActionBar().setTitle(sTitle);
            }
        });

    }

    private void load(String urlToLoad) {
        pd = new ProgressDialog(NoticeDetails.this);
        pd.setMessage("Please wait. Loading...");
        pd.show();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.loadUrl(urlToLoad);
        webView.setWebViewClient(new MyWebViewClient());
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url != null && url.startsWith(GATE_WAY)) {
                view.loadUrl(url);

            } else{
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                return true;
            }


            if (!pd.isShowing()) {
                pd.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }


        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            webView.loadUrl("file:///android_asset/i.html");

        }
    }


}
