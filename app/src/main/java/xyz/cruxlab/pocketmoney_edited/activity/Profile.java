package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.fragment.BonusFragment;
import xyz.cruxlab.pocketmoney_edited.fragment.EarningFragment;
import xyz.cruxlab.pocketmoney_edited.fragment.ProfileFragment;
import xyz.cruxlab.pocketmoney_edited.fragment.ReferralFragmant;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.FORGOT_PIN;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class Profile extends AppCompatActivity {

    LocalData localData;
    TextView name, currentPoint, totalPoint;
    Button forgot_pin;
    Context context;
    private ProgressDialog progressDialog;
    private int statusCode;


    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        context = getApplicationContext();
        localData = new LocalData(getApplicationContext());


        name = findViewById(R.id.nameProfile);
        currentPoint = findViewById(R.id.currentPoint_profile);
        totalPoint = findViewById(R.id.totalPoint_profile);

        name.setText(localData.getName());
        currentPoint.setText(localData.getcPoint() + "\nCurrent Point");
        totalPoint.setText(localData.getTPoint() + "\nTotal Point");


//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(getApplicationContext(), Setting.class);
                startActivity(intent);
            }
        });

        CircleImageView profilePic = (CircleImageView) findViewById(R.id.profilePic);
        String proPic = localData.getPhotoUrl();

        //Log.d("GoogleActivity", proPic);
        if (!proPic.isEmpty()) {
            Picasso.with(getApplicationContext()).load(proPic).into(profilePic);
        }


        forgot_pin = findViewById(R.id.forgot_pin_button);
        forgot_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog_ForgotPIN();
            }
        });


    }

    private void showDialog_ForgotPIN() {


        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("Forgot PIN?");
        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_yellow));
        title.setTextColor(getResources().getColor(R.color.black));

        content.setText(Html.fromHtml("If you forgot your PIN, request here. We'll send your pin to your email.\nRequest NOW?"));
        Button request = myDialogue.findViewById(R.id.button);
        request.setText("Request For PIN");

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();

                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    new RequestForPIN().execute();

                } else {
                    Toast.makeText(Profile.this, "No network available!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {

                case 0:
                    return new ProfileFragment();
                case 1:
                    return new EarningFragment();
                case 2:
                    return new ReferralFragmant();
//                case 3:
//                    return new BonusFragment();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    private class RequestForPIN extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Profile.this, "Requesting", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + FORGOT_PIN);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (Profile.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                    return;
                }
            }

            //Log.d("mainJson", result);

            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);

                    statusCode = mainJson.getInt(STATUS);

                    if (statusCode == STATUS_OKAY) {

                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ERROR) {

                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {

                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে মেইল ও রেফারেল কি সহ ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

            if (!Profile.this.isFinishing() && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }


    private class GetDataFromUrl {

        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {


            //Log.d("status -", localData.getUserID() + " " + LocalData.getIdentifier(getApplicationContext()));
            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }
}
