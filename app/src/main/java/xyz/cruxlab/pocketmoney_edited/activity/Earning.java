package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.services.BroadcastService;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.services.TimeTracker;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.ERROR_SESSION_RUNNING;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.INDEX;
import static xyz.cruxlab.pocketmoney_edited.C.MAX_POINT;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.POINT_ALREADY_ADDED;
import static xyz.cruxlab.pocketmoney_edited.C.SESSION;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.UPDATE_POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

public class Earning extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    InterstitialAd interstitialAd, interstitialAd1, interstitialAd2, interstitialAd3, interstitialAd4, interstitialAd5;
    InterstitialAd mInterstitialAd;

    private Button ckPoints, reloadAds, cricket, garden_gnome, harry_Potter;
    Button[] buttonList = new Button[26];

    Context context;

    private int index;
    private String MY_PREFS_NAME = "INDEX_PREF";
    private String INDEX_NAME = "INDEX_STRING";
    private String ADSClick_NAME = "ads_STRING";

    private int count = 36;// todo
    Timer T;

    private ProgressDialog progressDialog;
    private int statusCode;
    CardView waitingTimer;
    TextView noticeBoard;
    long savedDate;

    TickerView currentPoints;
    int point = 1;// todo
    String savedDate_lastTime;

    //AlarmManager am;
    public static final String MY_ACTION = "com.sample.myaction";


    boolean adsRemainToday = true;
    private boolean is30MinRunning = false;
    private boolean is30SecRunning = false;
    //private boolean adsClicked = false;
    boolean askedToClick = false;
    //
    private long currentTime;
    //
    private long savedTime, savedSec;
    //
    private int MAX_ADS_NUMBER = 25;
    int dailySession;

    boolean adsLoaded = false;
    LocalData localData;
    public static int thirtyMin = 29 * 60; // todo

    //public static String TAG = "Earningxxxx";
    int something = 0;

    StateProgressBar stateProgressBar;

    String TAG = "Fuckxxx";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = getApplicationContext();
        localData = new LocalData(context);


        //MobileAds.initialize(this, context.getResources().getString(R.string.app_id));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initVars();

        // init ads

        savedDate = localData.getTime();
        setAdsClick(false);

        //getting 30 min +sec
        is30MinRunning = min30Status();
        is30SecRunning = sec30Status();


        //Log.d(TAG, is30MinRunning + " " + is30SecRunning);

        if (is30MinRunning) {
            // show 30 min timer

            currentTime = System.currentTimeMillis() / 1000;
            long savedTime = localData.getTime();
            int timedif = (int) (currentTime - savedTime);
            min30Timer(thirtyMin - timedif);


        } else if (is30SecRunning) {
            // show 30sec timer

            currentTime = System.currentTimeMillis() / 1000;
            long savedTime = localData.getSec30();
            int timedif = (int) (currentTime - savedTime);
            count = 36 - timedif;
            setTimer30Sec();

        } else {

            dailySessionCheck();
        }


        if (adsRemainToday && (!is30MinRunning)) {

            for (Button aButtonList : buttonList) {
                aButtonList.setOnClickListener(this);
            }

            loadAds();


        } else {
            noAdsTodayNotification();
        }

        updatePoints();


        ckPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAdsClick(false);
                startActivity(new Intent(context, Withdrawals.class));
            }
        });

        reloadAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, Earning.class));
                finish();
            }
        });


        cricket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadGame(1);
            }
        });
    }

    private void loadAds() {

        index = getIndex();
        if (index != 0) {
            for (int i = 0; i < getIndex(); i++) {
                buttonList[i].setEnabled(true);
                buttonList[i].setText("✓");
            }
        }

        noticeBoard.setText("Loading Ads...");

        if ((index == 6) || (index == 12) || (index == 19) || (index == 25)) {
            askedToClick = true;
            loadHIM();

        } else {
            askedToClick = false;
            LoadAdsExtra(index);
        }

    }

    private void loadGame(int i) {

        WebView wv;
        wv = (WebView) findViewById(R.id.webView);
        wv.setVisibility(View.VISIBLE);
        wv.getSettings().setJavaScriptEnabled(true);

        if (i == 1)
            wv.loadUrl("file:///android_asset/cricket/game.html");

//        if (i == 2)
//            wv.loadUrl("file:///android_asset/flappy/index.html");

    }

    private boolean min30Status() {

        currentTime = System.currentTimeMillis() / 1000;
        savedTime = localData.getTime();

        return (currentTime - savedTime) < (thirtyMin); // 30*60 = 30 min

    }

    private boolean sec30Status() {

        currentTime = System.currentTimeMillis() / 1000;
        savedSec = localData.getSec30();

        return (currentTime - savedSec) < (30); // 30 sec

    }


    private void dailySessionCheck() {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String today = df.format(c);
        savedDate_lastTime = localData.getSavedDate();


        if (savedDate_lastTime.equals(today)) {

            dailySession = localData.getRemainDailySession();
            //Log.d(TAG, savedDate_lastTime.equals(today) + " " + dailySession);

            if (dailySession >= 6) {
                // no ads today
                adsRemainToday = false;
                noAdsTodayNotification();

            } else {
                adsRemainToday = true;
            }

        } else {


            localData.setRemainDailySession(1);
            setIndex(0);
            localData.setSavedDate(today);
            adsRemainToday = true;

            showTips();
        }

        //Log.d(TAG, " baki ache" + localData.getRemainDailySession());

    }


    public void setTimer30Sec() {


        index = getIndex();
        if ((index == 6) || (index == 12) || (index == 19) || (index == 25)) {
            askedToClick = true;
            loadHIM();

        } else {
            askedToClick = false;

            LoadAdsExtra(index);

        }


        T = new Timer();

        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        noticeBoard.setText("Please Wait (" + count + ")");
                        count--;
                        is30SecRunning = true;

                        if (count < 1) {
                            T.cancel();
                            noticeBoard.setText("অ্যাড দেখার জন্য প্রস্তুত।");
                            buttonList[index].setEnabled(true);
                            is30SecRunning = false;

                            //playNotificationSound();

                        }

                    }

//                    private void playNotificationSound() {
//
//                        try {
//                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
//                            r.play();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
                });
            }
        }, 1000, 1000);


    }

    private void noAdsTodayNotification() {

        noticeBoard.setText(getResources().getString(R.string.ads_end_for_today));
    }


    private void updatePoints() {

        currentPoints.setText("Today's Earning: " + localData.getTodaysPoint());
    }

    private void min30Timer(int sec) {

        is30MinRunning = true;
        Intent serviceIntent = new Intent(this, BroadcastService.class);

        serviceIntent.putExtra("time", sec);
        startService(serviceIntent);

    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
        }
    };

    private void updateGUI(Intent intent) {

        if (intent.getExtras() != null) {
            long millisUntilFinished = intent.getLongExtra("countdown", 0);

            if (millisUntilFinished > 2000) {
                is30MinRunning = true;
                //showAds.setEnabled(false);
                noticeBoard.setText("Please Wait: " + Utils.secToMinSec(millisUntilFinished / 1000));

            } else {

                //showAds.setEnabled(true);
                noticeBoard.setText("ব্যাকে গিয়ে আবার এখানে আসুন");
                is30MinRunning = false;

                if (Utils.isMyServiceRunning(context, BroadcastService.class))
                    stopService(new Intent(this, BroadcastService.class));
            }
        }
    }

//    public void setOneTimeAlarm(int sec) {
//
//        Intent intent = new Intent(MY_ACTION);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (sec * 1000), pendingIntent);
//    }


    private void loadHIM() {

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

                noticeBoard.setText("এই অ্যাডে ক্লিক করে ২৫ পয়েন্ট নিন");
                buttonList[getIndex()].setEnabled(true);

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                Toast.makeText(context, "Failed to load Ads.\nTry again.", Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();

                //adsClicked = true;
                setAdsClick(true);
                Toast.makeText(context, "মিনিমাম ৩০ সেকেন্ড পরে ব্যাক করবেন।", Toast.LENGTH_LONG).show();
                startService(new Intent(context, TimeTracker.class));


            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();


                if (getAdsClick() == false && ((index == 6) || (index == 12) || (index == 19) || (index == 25))) {

                    index++;
                    setIndex(index);

                    if (index <= MAX_ADS_NUMBER) {

                        localData.setSec30(System.currentTimeMillis());
                        count = 35;
                        setTimer30Sec();

                    } else {

                        for (Button b : buttonList) {
                            b.setEnabled(false);
                        }

                        // one circle complete
                        dailySession = localData.getRemainDailySession() + 1;
                        localData.setRemainDailySession(dailySession);

                        if (dailySession == 6) {
                            index = 0;
                            setIndex(index);
                            adsRemainToday = false;
                            noAdsTodayNotification();
                            dailySessionCheck();

                        } else {
                            index = 0;
                            setIndex(index);

                            localData.setTime(System.currentTimeMillis());
                            min30Timer(thirtyMin);

//                            am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//                            setOneTimeAlarm(thirtyMin);
                        }
                    }

                    setAdsClick(false);


                    point = 1;
                    if (Utils.isNetworkAvailable(context)) {
                        new UpdatePoints().execute();
                    } else {
                        Toast.makeText(context, "ইন্টারনেট সংযোগ নেই। পয়েন্ট যোগ করা যায়নি।", Toast.LENGTH_SHORT).show();
                    }
                }

            }

        });

    }


    private int getIndex() {

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return prefs.getInt(INDEX_NAME, 0); // TODo
    }

    private void setIndex(int index) {

        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(INDEX_NAME, index);
        editor.apply();
    }

    private boolean getAdsClick() {

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return prefs.getBoolean(ADSClick_NAME, false);
    }

    private void setAdsClick(boolean state) {

        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(ADSClick_NAME, state);
        editor.apply();
    }


    private void initVars() {

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "GameActivity");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        //ins
        mInterstitialAd = new InterstitialAd(context);
        //TODO
        mInterstitialAd.setAdUnitId(localData.getAds1());
        //mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/8691691433"); // test

        // video
//        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
//        mRewardedVideoAd.setRewardedVideoAdListener(this);


        //showAds = findViewById(R.id.showAds);
        waitingTimer = (CardView) findViewById(R.id.waitingTime);
        noticeBoard = findViewById(R.id.noticeBoard);

        currentPoints = findViewById(R.id.currentPoints);
        currentPoints.setCharacterLists(TickerUtils.provideNumberList());
        currentPoints.setText("Total Earning: 0");
        currentPoints.setAnimationDuration(1000);

        ckPoints = findViewById(R.id.ckPoints);
        reloadAds = findViewById(R.id.reloadActivity);

        cricket = findViewById(R.id.cricket);

        garden_gnome = findViewById(R.id.garden_gnome);
        harry_Potter = findViewById(R.id.harry_Potter);


        garden_gnome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Games.class);
                intent.putExtra("gameUrl", "https://www.google.com/logos/2018/gnomes/gnomes18.html?hl=en\n");
                startActivity(intent);
            }
        });
        harry_Potter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Games.class);
                intent.putExtra("gameUrl", "https://www.google.com/logos/2016/halloween16/halloween16.html?hl=en");
                startActivity(intent);
            }
        });


        // init button
        buttonList[0] = findViewById(R.id.button1);
        buttonList[1] = findViewById(R.id.button2);
        buttonList[2] = findViewById(R.id.button3);
        buttonList[3] = findViewById(R.id.button4);
        buttonList[4] = findViewById(R.id.button5);
        buttonList[5] = findViewById(R.id.button6);
        buttonList[6] = findViewById(R.id.button7);
        buttonList[7] = findViewById(R.id.button8);
        buttonList[8] = findViewById(R.id.button9);
        buttonList[9] = findViewById(R.id.button10);
        buttonList[10] = findViewById(R.id.button11);
        buttonList[11] = findViewById(R.id.button12);
        buttonList[12] = findViewById(R.id.button13);
        buttonList[13] = findViewById(R.id.button14);
        buttonList[14] = findViewById(R.id.button15);
        buttonList[15] = findViewById(R.id.button16);
        buttonList[16] = findViewById(R.id.button17);
        buttonList[17] = findViewById(R.id.button18);
        buttonList[18] = findViewById(R.id.button19);
        buttonList[19] = findViewById(R.id.button20);
        buttonList[20] = findViewById(R.id.button21);
        buttonList[21] = findViewById(R.id.button22);
        buttonList[22] = findViewById(R.id.button23);
        buttonList[23] = findViewById(R.id.button24);
        buttonList[24] = findViewById(R.id.button25);
        buttonList[25] = findViewById(R.id.button26);

        stateProgressBar = (StateProgressBar) findViewById(R.id.state_progress_bar);

        int stateNo = localData.getRemainDailySession();
        if (stateNo == 5) {
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FIVE);
        } else if (stateNo == 4) {
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
        } else if (stateNo == 2) {
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
        } else if (stateNo == 1) {
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
        } else if (stateNo == 3) {
            stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
        }


        //Toast.makeText(context, stateNo + " progress", Toast.LENGTH_SHORT).show();

        AdView mAdView = findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, BroadcastService.class));

        if (progressDialog != null && progressDialog.isShowing()) {

            progressDialog.dismiss();
        }


        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        //mRewardedVideoAd.resume(this);
        registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));

        //Toast.makeText(context, , Toast.LENGTH_SHORT).show();
        //Log.d("fuckxx", getIndex() + " " + index + " index - clicked call everytime" + adsClicked);
        //Log.d("fuckxx", localData.getRemainDailySession() + " dailySession");


        if (getAdsClick() && ((index == 6) || (index == 12) || (index == 19) || (index == 25))) {

//          if (askedToClick) {
            index++;
            setIndex(index);

            if (TimeTracker.timerEnd) {

                point = 25;
                if (Utils.isNetworkAvailable(context)) {
                    new UpdatePoints().execute();
                } else {
                    Toast.makeText(context, "ইন্টারনেট সংযোগ নেই। পয়েন্ট যোগ করা যায়নি।", Toast.LENGTH_SHORT).show();
                }

            } else {
                showWarning30Sec();

                point = 1;
                if (Utils.isNetworkAvailable(context)) {
                    new UpdatePoints().execute();
                } else {
                    Toast.makeText(context, "ইন্টারনেট সংযোগ নেই। পয়েন্ট যোগ করা যায়নি।", Toast.LENGTH_SHORT).show();
                }
            }

            if (index <= MAX_ADS_NUMBER) {

                localData.setSec30(System.currentTimeMillis());
                count = 36;
                setTimer30Sec();

            } else {


                for (Button b : buttonList) {
                    b.setEnabled(false);
                }

                // one circle complete
                dailySession = localData.getRemainDailySession() + 1;
                localData.setRemainDailySession(dailySession);

                if (dailySession == 6) {
                    index = 0;
                    setIndex(index);
                    adsRemainToday = false;
                    noAdsTodayNotification();
                    dailySessionCheck();

                } else {
                    index = 0;
                    setIndex(index);

                    localData.setTime(System.currentTimeMillis());
                    min30Timer(thirtyMin);
//                    am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//                    setOneTimeAlarm(thirtyMin);
                }
            }

            setAdsClick(false);
        }


    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == R.id.button1) {
            showMultiple(0);
        }
        if (id == R.id.button2) {
            showMultiple(1);
        }
        if (id == R.id.button3) {
            showMultiple(2);
        }
        if (id == R.id.button4) {
            showMultiple(3);
        }
        if (id == R.id.button5) {
            showMultiple(4);
        }
        if (id == R.id.button6) {
            showMultiple(5);
        }
        if (id == R.id.button7) {
            showSingle(6);
        }
        if (id == R.id.button8) {
            showMultiple(7);
        }
        if (id == R.id.button9) {
            showMultiple(8);
        }
        if (id == R.id.button10) {
            showMultiple(9);
        }
        if (id == R.id.button11) {
            showMultiple(10);
        }
        if (id == R.id.button12) {
            showMultiple(11);
        }
        if (id == R.id.button13) {
            showSingle(12);
        }
        if (id == R.id.button14) {
            showMultiple(13);
        }
        if (id == R.id.button15) {
            showMultiple(14);
        }
        if (id == R.id.button16) {
            showMultiple(15);
        }
        if (id == R.id.button17) {
            showMultiple(16);
        }
        if (id == R.id.button18) {
            showMultiple(17);
        }
        if (id == R.id.button19) {
            showMultiple(18);
        }
        if (id == R.id.button20) {
            showSingle(19);
        }
        if (id == R.id.button21) {
            showMultiple(20);
        }
        if (id == R.id.button22) {
            showMultiple(21);
        }
        if (id == R.id.button23) {
            showMultiple(22);
        }
        if (id == R.id.button24) {
            showMultiple(23);
        }
        if (id == R.id.button25) {
            showMultiple(24);
        }
        if (id == R.id.button26) {
            showSingle(25);
        }
    }

    private void showSingle(int position) {

        int i = getIndex();

        if (is30SecRunning || is30MinRunning) {
            Toast.makeText(context, "Timer is running. Please Wait...", Toast.LENGTH_SHORT).show();

        } else {
            if (position < i) {
                Toast.makeText(context, "ইতোমধ্যে ক্লিক করেছেন", Toast.LENGTH_SHORT).show();

            } else {
                showNoticeDialog(position);
            }
        }


    }

    private void showMultiple(int position) {

        int i = getIndex();

        if (is30SecRunning || is30MinRunning) {
            Toast.makeText(context, "Timer is running. Please Wait...", Toast.LENGTH_SHORT).show();
        } else {

            if (position < i) {
                Toast.makeText(context, "ইতোমধ্যে ক্লিক করেছেন", Toast.LENGTH_SHORT).show();
            } else {
                showAdsExtra();
                buttonList[position].setText("✓");

                index++;
                setIndex(index);
            }
        }
    }


    private class UpdatePoints extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                if (Earning.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
            if (!Earning.this.isFinishing()) {
                progressDialog = ProgressDialog.show(Earning.this, "Adding Points", "Earn point, get money!", true, false);
            }
//                }
//            }
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl1 getData = new GetDataFromUrl1();
            String response = null;
            try {
                response = getData.run(GATE_WAY + UPDATE_POINTS);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);


                    if (statusCode == STATUS_OKAY) {

                        int cPoint = Integer.parseInt(localData.getTodaysPoint());
                        localData.setTodaysPoint((cPoint + point) + "");
                        updatePoints();

                        Toast.makeText(context, point + " পয়েন্ট যোগ হয়েছে!", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == MAX_POINT) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == ERROR_SESSION_RUNNING) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ERROR) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == POINT_ALREADY_ADDED) {
                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {

                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {

                        Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (!Earning.this.isFinishing())
                    retryPointAdd();
            }

            if (progressDialog != null) {

                if (!Earning.this.isFinishing() && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }


        }

    }

    private class GetDataFromUrl1 {

        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {

            LocalData localData = new LocalData(context);

            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .add(POINTS, point + "")
                    .add(INDEX, index+"")
                    .add(SESSION, dailySession+"")
                    .build();

            //Log.d("details", localData.getUserID() + " " + LocalData.getIdentifier(getApplicationContext()) + " " + nameString + " " + pinString + " " + withDrawlMethod + " " + withDrawlNumberString);

            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }

    private void showWarning30Sec() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("Warning");
        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_yellow));
        title.setTextColor(getResources().getColor(R.color.black));

        content.setText(Html.fromHtml("আপনি অ্যাড এ ক্লিক করে <strong>৩০ সেকেন্ডের কম সময়</strong> ছিলেন। <br>আপনাকে <strong>মিনিমাম ৩০ সেকেন্ড</strong> থাকতে হবে অন্যথায় <strong>পয়েন্ট পাবেন না।<strong><br>"));


        myDialogue.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();

            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();
    }

    private void showTips() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("Tips");
        TextView content = (TextView) myDialogue.findViewById(R.id.message);
        content.setGravity(Gravity.LEFT);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_green));
        title.setTextColor(getResources().getColor(R.color.white));

        content.setText(Html.fromHtml("১. সবসময় VPN ব্যবহারের চেষ্টা করবেন। নরমালি Ads Load হলেও।<br><br>২. ডাটা ক্লিয়ার করে মিস হওয়া পয়েন্ট নেবেন না।<br><br>৩. Google Map, Location, Chrome ইত্যাদি অ্যাপ Closed/Force Stop করে আর্ন করুন।"));


        myDialogue.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();

            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();
    }


    private void retryPointAdd() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("Error!!!");

        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_red));
        title.setTextColor(getResources().getColor(R.color.white));

        content.setText(Html.fromHtml("পয়েন্ট অ্যাড করা যায়নি।  <br>এটা সাধারণত ইন্টারনেট এ সমস্যার করনে হয়। কানেকশন আছে কিনা চেক করে আবার চেস্টা করুন।"));
        Button retry = (Button) myDialogue.findViewById(R.id.button);
        retry.setText("Try Again");


        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();
                //updateIndex();

                if (Utils.isNetworkAvailable(context)) {
                    new UpdatePoints().execute();
                } else {
                    Toast.makeText(context, "ইন্টারনেট সংযোগ নেই। পয়েন্ট যোগ করা যায়নি।", Toast.LENGTH_SHORT).show();
                }

            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();
    }

    private void showNoticeDialog(final int position) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);


        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("লক্ষ্য করুন");

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        TextView content = (TextView) myDialogue.findViewById(R.id.message);


        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_red));
        title.setTextColor(getResources().getColor(R.color.white));
        content.setText(Html.fromHtml("অ্যাড আসা মাত্রই ক্লিক করবেন না।<br> একটু সময় অপেক্ষা করে অ্যাডে <b>ক্লিক করুন</b> এবং <b>মিনিমাম ৩০ সেকেন্ড</b> ওই পেজে থাকুন। <br><br><small>কোন ওয়েব পেজে গেলে <strong>সম্পুর্ণ লোড</strong> হওয়া পর্যন্ত অপেক্ষা করুন। <br>কোন অ্যাপস ইন্সটল করা <b>বাধতামুলক নয়</b>।</small>"));

        myDialogue.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    buttonList[position].setText("✓");


                } else {
                    Toast.makeText(context, "Ads is not loaded.\nPlease wait a moment.", Toast.LENGTH_SHORT).show();
                }


            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();

    }


    public void LoadAdsExtra(final int indexPosition) {

        something = 0;

        interstitialAd = new InterstitialAd(context);
        interstitialAd1 = new InterstitialAd(context);
        interstitialAd2 = new InterstitialAd(context);
        interstitialAd3 = new InterstitialAd(context);
        interstitialAd4 = new InterstitialAd(context);


//        // test id
//        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
//        interstitialAd1.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
//        interstitialAd2.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
//        interstitialAd3.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
//        interstitialAd4.setAdUnitId("ca-app-pub-3940256099942544/1033173712");


        // original id

        interstitialAd.setAdUnitId(localData.getAds1());
        interstitialAd1.setAdUnitId(localData.getAds1());
        interstitialAd2.setAdUnitId(localData.getAds1());
        interstitialAd3.setAdUnitId(localData.getAds1());
        interstitialAd4.setAdUnitId(localData.getAds1());


//        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/8691691433");
//        interstitialAd1.setAdUnitId("ca-app-pub-3940256099942544/8691691433");
//        interstitialAd2.setAdUnitId("ca-app-pub-3940256099942544/8691691433");
//        interstitialAd3.setAdUnitId("ca-app-pub-3940256099942544/8691691433");
//        interstitialAd4.setAdUnitId("ca-app-pub-3940256099942544/8691691433");

        //interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/8691691433"); // tst

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        interstitialAd.loadAd(adRequest);


        AdRequest adRequest1 = new AdRequest.Builder()
                .build();
        interstitialAd1.loadAd(adRequest1);


        AdRequest adRequest2 = new AdRequest.Builder()
                .build();
        interstitialAd2.loadAd(adRequest2);


        AdRequest adRequest3 = new AdRequest.Builder()
                .build();
        interstitialAd3.loadAd(adRequest3);


        AdRequest adRequest4 = new AdRequest.Builder()
                .build();
        interstitialAd4.loadAd(adRequest4);


        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                buttonList[getIndex()].setEnabled(true);

                setButtonEnable(indexPosition);
                noticeBoard.setText("অ্যাড লোড হয়েছে।");
                //newAdsRequest();
                adsLoaded = true;

                if (something < 5)
                    something++;

                //Log.d(TAG, something + " ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();

                something--;
                newAdsRequest();
                //Log.d(TAG, something + " ");
            }

            @Override
            public void onAdFailedToLoad(int code) {
                super.onAdFailedToLoad(code);
                noticeBoard.setText("Failed to load. Try again. " + code);
            }
        });
        interstitialAd1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                buttonList[getIndex()].setEnabled(true);
                if (something < 5)
                    something++;
                setButtonEnable(indexPosition);
                noticeBoard.setText("অ্যাড লোড হয়েছে।");
                //newAdsRequest();

                adsLoaded = true;
                //Log.d(TAG, something + " ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();

                something--;
                newAdsRequest();
                //Log.d(TAG, something + " ");
            }

        });
        interstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                buttonList[getIndex()].setEnabled(true);
                if (something < 5)
                    something++;
                setButtonEnable(indexPosition);
                noticeBoard.setText("অ্যাড লোড হয়েছে।");
                //newAdsRequest();

                adsLoaded = true;
                //Log.d(TAG, something + " ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();

                something--;
                newAdsRequest();
                //Log.d(TAG, something + " ");
            }

        });
        interstitialAd3.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                buttonList[getIndex()].setEnabled(true);
                if (something < 5)
                    something++;
                setButtonEnable(indexPosition);
                noticeBoard.setText("অ্যাড লোড হয়েছে।");
                //newAdsRequest();

                adsLoaded = true;
                //Log.d(TAG, something + " ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();

                something--;
                newAdsRequest();
                //Log.d(TAG, something + " ");
            }

        });
        interstitialAd4.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                buttonList[getIndex()].setEnabled(true);
                if (something < 5)
                    something++;
                setButtonEnable(indexPosition);
                noticeBoard.setText("অ্যাড লোড হয়েছে।");
                //newAdsRequest();

                adsLoaded = true;
                //Log.d(TAG, something + " ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();

                something--;
                newAdsRequest();
                //Log.d(TAG, something + " ");
            }

        });


    }

    private void setButtonEnable(int indexPosition) {
        buttonList[indexPosition].setEnabled(true);
    }

    private void newAdsRequest() {

        //Log.d(TAG, something + " something");

        // something will prevent multiple 1 point adds
        if (something <= 0) {

            if (adsLoaded) {

                something = 0;
                point = 1;
                if (Utils.isNetworkAvailable(context)) {
                    new UpdatePoints().execute();
                } else {
                    Toast.makeText(context, "ইন্টারনেট সংযোগ নেই। পয়েন্ট যোগ করা যায়নি।", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(context, "অ্যাড লোডে সমস্যা হয়েছে।", Toast.LENGTH_SHORT).show();
            }


            localData.setSec30(System.currentTimeMillis());
            //Random r = new Random();
            count = 36;// r.nextInt(35 - 25) + 25;
            currentTime = System.currentTimeMillis() / 1000;

            setTimer30Sec();

        }
    }

    public void showAdsExtra() {
        if (!Earning.this.isFinishing()) {
            if (interstitialAd.isLoaded())
                interstitialAd.show();

            if (interstitialAd1.isLoaded())
                interstitialAd1.show();

            if (interstitialAd2.isLoaded())
                interstitialAd2.show();

            if (interstitialAd3.isLoaded())
                interstitialAd3.show();

            if (interstitialAd4.isLoaded())
                interstitialAd4.show();

        }
//        else {
//            Toast.makeText(context, "You are not in Earning Page", Toast.LENGTH_SHORT).show();
//        }


    }


}