package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.services.BroadcastService;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.menu_card.CardAdapter;
import xyz.cruxlab.pocketmoney_edited.menu_card.CardMenu;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.ACC_CREATED;
import static xyz.cruxlab.pocketmoney_edited.C.AC_STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.ADS1;
import static xyz.cruxlab.pocketmoney_edited.C.ADS2;
import static xyz.cruxlab.pocketmoney_edited.C.APP_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.APP_URL;
import static xyz.cruxlab.pocketmoney_edited.C.BADGE;
import static xyz.cruxlab.pocketmoney_edited.C.CURRENT_POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.DEVICE_NAME;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.MAIN;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.NAME;
import static xyz.cruxlab.pocketmoney_edited.C.NOTICE_DETAILS;
import static xyz.cruxlab.pocketmoney_edited.C.NOTICE_ID;
import static xyz.cruxlab.pocketmoney_edited.C.NOTICE_TEXT;
import static xyz.cruxlab.pocketmoney_edited.C.PIN;
import static xyz.cruxlab.pocketmoney_edited.C.REFERRAL_CODE;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.UPDATE_TYPE;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAW_METHOD;
import static xyz.cruxlab.pocketmoney_edited.activity.Earning.thirtyMin;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    private ProgressDialog progressDialog;
    private int statusCode;
    private TextView noticeBoard;
    long savedDate;
    Context context;
    LocalData localData;
    CircleImageView profilePic;

    private String nameString, pinString, currentPointString;
    TextView nameOnNav, currentPointOnNav;
    TickerView currentPoint;
    PackageManager manager;

    String appLink, accCreated, refCode;
    int appCode, update_type = 0;
    //private GoogleSignInClient mGoogleSignInClient;
    private boolean isBroadcastRegistered = false;
    NavigationView navigationView;

    int notice_id;
    CardView notice_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        context = getApplicationContext();
        localData = new LocalData(context);
        savedDate = localData.getTime();
        checkTime();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        selectMainMenuOnNav();

        View headerView = navigationView.getHeaderView(0);
        nameOnNav = (TextView) headerView.findViewById(R.id.nameOnNav);
        currentPointOnNav = (TextView) headerView.findViewById(R.id.pointsOnNav);
        profilePic = (CircleImageView) headerView.findViewById(R.id.profilePic);
        String proPic = localData.getPhotoUrl();

        //Log.d("GoogleActivity", proPic);
        if (!proPic.isEmpty()) {
            Picasso.with(context).load(proPic).into(profilePic);
        }


        currentPoint = findViewById(R.id.currentPoints);
        currentPoint.setCharacterLists(TickerUtils.provideNumberList());
        currentPoint.setText("0");
        currentPoint.setAnimationDuration(1000);

        Fabric.with(this, new Crashlytics());
        //Crashlytics.getInstance().crash(); // Force a crash


        noticeBoard = findViewById(R.id.noticeBoard);
        noticeBoard.setSelected(true);
        marqueeText("Earn from Your Android Phone!");
        notice_card = findViewById(R.id.notice_card);
        notice_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notice_id != 0) {
                    Intent intent = new Intent(getApplicationContext(), NoticeDetails.class);
                    intent.putExtra("id", notice_id + "");
                    startActivity(intent);
                }
            }
        });

        boolean loggedIn = localData.getLoggedIn();
        if (!loggedIn) {
            Intent intent = new Intent(getApplicationContext(), LoginActivityGmail.class);
            startActivity(intent);
            finish();
        }

//        else {
//
////            if (!Utils.isNetworkAvailable(context)) {
////                //new LoadDataFromServer().execute();
////                showWarningNoInternet();
////            }
//
//            // is first time or not
//            boolean oldUser = localData.getFirstTime();
//            if (!oldUser) {
//                startActivity(new Intent(context, Tutorial.class));
//
//            }
//        }

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "MainActivity");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        List<CardMenu> list = new ArrayList<>();

        list.add(new CardMenu(R.drawable.earn_money, "Earning", 1));
        //list.add(new CardMenu(R.drawable.gift, "রেফারেল", 2));
        list.add(new CardMenu(R.drawable.profile_icon, "Profile", 2));
        list.add(new CardMenu(R.drawable.withdrawl, "Withdrawal", 3));

        list.add(new CardMenu(R.drawable.leaderboard, "LeaderBoard", 4));
        list.add(new CardMenu(R.drawable.payment_hist, "Payment Hist", 5));
        list.add(new CardMenu(R.drawable.necessary_links, "Notice", 6));

        //list.add(new CardMenu(R.drawable.tutorials, "ব্যবহারবিধি", 5));
        //list.add(new CardMenu(R.drawable.about, "বিস্তারিত", 6));


        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        CardAdapter adapter = new CardAdapter(getApplicationContext(), list);
        recyclerView.setAdapter(adapter);


    }

    private void selectMainMenuOnNav() {
        navigationView.getMenu().getItem(0).setChecked(true);
    }


    private void checkTime() {

        long currentTime = System.currentTimeMillis() / 1000;
        long savedTime = localData.getTime();
        int timedif = (int) (currentTime - savedTime);

        if (timedif < thirtyMin) {

            Intent serviceIntent = new Intent(this, BroadcastService.class);
            serviceIntent.putExtra("time", thirtyMin - timedif);

            startService(serviceIntent);


        }

    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
        }
    };

    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
            long millisUntilFinished = intent.getLongExtra("countdown", 0);
            //Log.i(TAG, "Countdown seconds remaining: " +  millisUntilFinished / 1000);
            if (millisUntilFinished > 2000) {
                noticeBoard.setText("Please Wait: " + Utils.secToMinSec(millisUntilFinished / 1000));
            } else {
                noticeBoard.setText("Welcome to Pocket Money!");

                if (Utils.isMyServiceRunning(context, BroadcastService.class))
                    stopService(new Intent(this, BroadcastService.class));
            }
        }
    }


    public void marqueeText(String text) {
        noticeBoard.setText(text);
    }

    private class LoadDataFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this, "Checking data", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl getData = new GetDataFromUrl();
            String response = null;
            try {
                response = getData.run(GATE_WAY + MAIN);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (MainActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                    return;
                }
            }

            //Log.d("mainJson", result);

            if (result != null) {
                try {
                    JSONObject mainJson = new JSONObject(result);
                    statusCode = mainJson.getInt(STATUS);


                    if (statusCode == STATUS_OKAY) {
                        // name
                        nameString = mainJson.getString(NAME);
                        // pin
                        pinString = mainJson.getString(PIN);
                        // current point
                        currentPointString = mainJson.getString(CURRENT_POINTS);
                        //
                        appCode = mainJson.getInt(APP_CODE);
                        update_type = mainJson.getInt(UPDATE_TYPE);
                        appLink = mainJson.getString(APP_URL);
                        accCreated = mainJson.getString(ACC_CREATED);
                        refCode = mainJson.getString(REFERRAL_CODE);

                        notice_id = Integer.parseInt(mainJson.getString(NOTICE_ID));
                        marqueeText("Read: "+mainJson.getString(NOTICE_TEXT));


                        updateNamePoint();

                        manager = context.getPackageManager();
                        PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);


                        if ((info.versionCode < appCode)) {
                            if (!MainActivity.this.isFinishing()) {

                                appUpdateNoti(appLink, update_type);
                            }
                        }


                        localData.setName(nameString);
                        localData.setPin(pinString);
                        localData.setcPoint(currentPointString);
                        localData.setTodaysPoint(mainJson.getString("today_point"));
                        localData.setTPoint(mainJson.getString("total_point"));
                        localData.setAccCreated(accCreated);
                        localData.setRefCode(refCode);
                        localData.setWithDNumber(mainJson.getString("number"));
                        localData.setAppURL(appLink);

                        // saving ads unit
                        localData.setAds1(mainJson.getString(ADS1));
                        localData.setAds2(mainJson.getString(ADS2));

                        // more details
                        localData.setAccountStatus(mainJson.getInt(AC_STATUS));
                        localData.setWithdrawal_method(mainJson.getString(WITHDRAW_METHOD));
                        localData.setBadge(mainJson.getString(BADGE));

                        showWarningAccountBan(mainJson.getInt(AC_STATUS));


                    } else if (statusCode == STATUS_ERROR) {

                        Toast.makeText(context, "Can't Load data.", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_ACCOUNT_ERROR) {

                        Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে মেইল ও রেফারেল কি সহ ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                    } else if (statusCode == STATUS_SERVER_ERROR) {
                        Toast.makeText(context, mainJson.getInt(MESSAGE), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();
            }

            if (!MainActivity.this.isFinishing() && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    private void updateNamePoint() {

        if (!nameString.isEmpty())
            nameOnNav.setText(nameString);

        currentPoint.setText(currentPointString + "");
        currentPointOnNav.setText(currentPointString + " Point");
    }

    private class GetDataFromUrl {

        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {


            //Log.d("status -", localData.getUserID() + " " + LocalData.getIdentifier(getApplicationContext()));


            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .add(DEVICE_NAME, android.os.Build.MODEL + " - " + Build.BRAND)
                    .build();


            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_loggedOut) {

            LocalData localData = new LocalData(getApplicationContext());
            localData.setLoggedIn(false);
            startActivity(new Intent(context, LoginActivityGmail.class));
            finish();
            FirebaseAuth.getInstance().signOut();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_earning) {
            openIntent(Earning.class);
        } else if (id == R.id.profile_nav) {
            openIntent(Profile.class);
        } else if (id == R.id.nav_withdrawal) {
            openIntent(Withdrawals.class);

        } else if (id == R.id.leaderboard_nav) {
            openIntent(Leaderboard.class);
        } else if (id == R.id.payment_hist_nav) {
            openIntent(Payment_History.class);
        } else if (id == R.id.notice_nav) {
            openIntent(NoticeBoard.class);
        } else if (id == R.id.nav_fb_group) {

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/2149734491977371")));
            } catch (android.content.ActivityNotFoundException ignored) {
                Toast.makeText(context, "No browser found!", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_share_link) {

            try {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Pocket Money");
                String sAux = "Start Earning Today (Pocket Money)\n";
                sAux = sAux + localData.getAppURL() + "";
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "choose one"));
            } catch (Exception e) {
                //e.toString();
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openIntent(Class<?> activityName) {
        Intent intent = new Intent(context, activityName);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
        isBroadcastRegistered = true;
        checkTime();
        selectMainMenuOnNav();

        if (Utils.isNetworkAvailable(context)) {
            new LoadDataFromServer().execute();
        } else {
            showWarningNoInternet();
            noticeBoard.setText("ইন্টারনেট কানেক্ট করুন");
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(br);
        //Log.i(TAG, "Unregistered broacast receiver");
    }

    @Override
    public void onStop() {
//        try {
//            unregisterReceiver(br);
//        } catch (Exception e) {
//            // Receiver was probably already stopped in onPause()
//        }
        super.onStop();
    }

    @Override
    public void onDestroy() {

        if (isBroadcastRegistered)
            unregisterReceiver(br);
        if (Utils.isMyServiceRunning(context, BroadcastService.class))
            stopService(new Intent(this, BroadcastService.class));

        super.onDestroy();
    }

    private void showWarningNoInternet() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);

        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("লক্ষ্য করুন");

        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_red));
        title.setTextColor(getResources().getColor(R.color.white));

        content.setText(Html.fromHtml("ইন্টারনেট কানেকশন নেই। অ্যাপটি চালানোর জন্য ইন্টারনেট প্রয়োজন।"));


        myDialogue.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();
            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogue.show();
    }

    private void showWarningAccountBan(int type) {

        if (type != 0) {

            LayoutInflater factory = LayoutInflater.from(this);
            final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
            final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
            alertDialogue.setView(myDialogue);

            TextView title = (TextView) myDialogue.findViewById(R.id.title);
            title.setText("অ্যাকাউন্ট ব্যান হয়েছে");

            TextView content = (TextView) myDialogue.findViewById(R.id.message);

            RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
            titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_red));
            title.setTextColor(getResources().getColor(R.color.white));

            if (type == 1) {
                content.setText(Html.fromHtml("আপনার অ্যাকাউন্ট কিছু সময়ের জন্য ব্যান করা হয়েছে। এরপর সম্পুর্ন ব্যান হতে পারে\nদয়া করে ইল্লিগাল কাজ করবেন না।"));

            } else if (type == 2) {
                content.setText(Html.fromHtml("আপনার অ্যাকাউন্ট সম্পুর্নভাবে ব্যান করা হয়েছে। আমাদের অ্যাপটি ব্যবহার করার জন্য ধন্যবাদ।"));
                alertDialogue.setCancelable(false);
            }

            myDialogue.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogue.dismiss();
                }
            });

            alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            alertDialogue.show();
        }
    }

    private void appUpdateNoti(final String url, int type) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View myDialogue = factory.inflate(R.layout.dialogue_layout, null);
        final AlertDialog alertDialogue = new AlertDialog.Builder(this).create();
        alertDialogue.setView(myDialogue);

        TextView title = (TextView) myDialogue.findViewById(R.id.title);
        title.setText("নতুন আপডেট");

        TextView content = (TextView) myDialogue.findViewById(R.id.message);

        RelativeLayout titleBG = (RelativeLayout) myDialogue.findViewById(R.id.titleBackground);
        titleBG.setBackgroundColor(getResources().getColor(R.color.notification_title_green));
        title.setTextColor(getResources().getColor(R.color.white));

        content.setText(Html.fromHtml("অ্যাপের নতুন আপডেট এসেছে। কাজের জন্য আপনাকে বর্তমান অ্যাপটি আপডেট করে নিতে হবে।"));
        Button button = (Button) myDialogue.findViewById(R.id.button);
        button.setText("আপডেট করুন");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogue.dismiss();

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                } catch (android.content.ActivityNotFoundException ignored) {
                    Toast.makeText(context, "No browser found!", Toast.LENGTH_SHORT).show();
                }


            }
        });

        alertDialogue.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // if it is a mandatory update
        // 0 not mandatory, 1 = mandatory
        if (type == 1)
            alertDialogue.setCancelable(false);

        alertDialogue.show();
    }

}
