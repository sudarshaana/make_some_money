package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;

import static xyz.cruxlab.pocketmoney_edited.C.DAILY_MAX_LIMIT;
import static xyz.cruxlab.pocketmoney_edited.C.DAILY_SESSION_LIMIT;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LEADER_BOARD;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.UPDATE_POINTS;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;

class InitServer extends AsyncTask<String, Void, String> {

    Activity activity;
    private ProgressDialog progressDialog;
    private int statusCode;
    LocalData localData;
    Context context;

    public InitServer(Earning earning, Context context) {

        activity = earning;
        this.context = context;
        localData = new LocalData(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(activity, "Getting ready", "Checking Server status", true, false);
    }

    @Override
    protected String doInBackground(String... strings) {

        GetDataFromUrl1 getData = new GetDataFromUrl1(context);
        String response = null;

        try {
            response = getData.run(GATE_WAY + DAILY_MAX_LIMIT);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        Log.d("ResponseFuck", result);


        if (result != null) {
            try {
                Log.d("ResponseFuck", result + " <- fuck");
                JSONObject mainJson = new JSONObject(result);

                if (statusCode == STATUS_OKAY) {

                    Toast.makeText(context, "Yoy are ready to Go!", Toast.LENGTH_SHORT).show();

                } else if (statusCode == STATUS_ERROR) {

                    Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                } else if (statusCode == STATUS_ACCOUNT_ERROR) {

                    Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                } else if (statusCode == STATUS_SERVER_ERROR) {

                    Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, "সার্ভারে কানেক্ট করা যায়নি", Toast.LENGTH_SHORT).show();

        }


        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

}

class GetDataFromUrl1 {

    OkHttpClient client = new OkHttpClient();
    Context context;

    public GetDataFromUrl1(Context context) {
        this.context = context;
    }

    String run(String url) throws IOException {

        LocalData localData = new LocalData(context);

        RequestBody formBody = new FormBody.Builder()
                .add(USER_ID, localData.getUserID() + "")
                .add(IDENTIFIER, LocalData.getIdentifier(context))
                .add(DAILY_SESSION_LIMIT, localData.getMaxAdsSession() + "")
                .build();

        //Log.d("Fuckxxx", localData.getMaxAdsSession() + " MAX Fuck init");
        //Log.d("details", localData.getUserID() + " " + LocalData.getIdentifier(getApplicationContext()) + " " + nameString + " " + pinString + " " + withDrawlMethod + " " + withDrawlNumberString);

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

}