package xyz.cruxlab.pocketmoney_edited.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.cruxlab.pocketmoney_edited.LocalData;
import xyz.cruxlab.pocketmoney_edited.R;
import xyz.cruxlab.pocketmoney_edited.firebase.SharedPrefManager;
import xyz.cruxlab.pocketmoney_edited.util.Utils;

import static xyz.cruxlab.pocketmoney_edited.C.FIREBASE_ID;
import static xyz.cruxlab.pocketmoney_edited.C.GATE_WAY;
import static xyz.cruxlab.pocketmoney_edited.C.IDENTIFIER;
import static xyz.cruxlab.pocketmoney_edited.C.LOGIN;
import static xyz.cruxlab.pocketmoney_edited.C.MESSAGE;
import static xyz.cruxlab.pocketmoney_edited.C.NAME;
import static xyz.cruxlab.pocketmoney_edited.C.PHONE_NO;
import static xyz.cruxlab.pocketmoney_edited.C.PIN;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ACCOUNT_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OKAY;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_OLD_USER;
import static xyz.cruxlab.pocketmoney_edited.C.STATUS_SERVER_ERROR;
import static xyz.cruxlab.pocketmoney_edited.C.UPDATE_PROFILE;
import static xyz.cruxlab.pocketmoney_edited.C.USER_ID;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAW_METHOD;
import static xyz.cruxlab.pocketmoney_edited.C.WITHDRAW_NO;

public class ProfileSetUp extends AppCompatActivity {
    private FirebaseAnalytics mFirebaseAnalytics;
    private ProgressDialog progressDialog;
    private int statusCode;
    private int userID;
    private EditText name, pin, withdrawalNumber;
    private String nameString, pinString, withDrawlNumberString, withDrawlMethod;
    private Spinner withDrawlMethodSpinner;
    private Button saveProfileButton;//, skipButton;
    private Context context;
    private LocalData localData;
    boolean successfully = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_set_up);

        context = getApplicationContext();
        localData = new LocalData(context);


        name = findViewById(R.id.nameEditText);
        pin = findViewById(R.id.pinEditText);
        withdrawalNumber = findViewById(R.id.wt_numberEditText);

        withDrawlMethodSpinner = findViewById(R.id.wt_method_spinner);
        saveProfileButton = findViewById(R.id.saveProfileButton);

        name.setText(localData.getName());

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ProfileActivity");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


//        if (Utils.isNetworkAvailable(getApplicationContext())) {
//            new LoadDataFromServer().execute();
//        } else
//            Toast.makeText(context, "ইন্টারনেট কানেকশন নেই।", Toast.LENGTH_SHORT).show();

        //PhoneNo_string = getIntent().getStringExtra(PHONE_NO);
        //Toast.makeText(this, PhoneNo_string, Toast.LENGTH_SHORT).show();


        saveProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (Utils.isNetworkAvailable(getApplicationContext())) {

                    nameString = name.getText().toString();
                    pinString = pin.getText().toString();
                    withDrawlNumberString = withdrawalNumber.getText().toString();


                    withDrawlMethod = withDrawlMethodSpinner.getSelectedItem().toString();

                    //Log.d("values", nameString + " " + pinString + " " + withDrawlNumberString + " " + withDrawlMethod);

                    if (isNull(nameString) || isNull(pinString) || isNull(withDrawlNumberString) || isNull(withDrawlMethod)) {
                        //Log.d("details",  nameString + " " + pinString + " " + withDrawlMethod + " " + withDrawlNumberString);
                        Toast.makeText(context, "সঠিকভাবে পুরন করুন।", Toast.LENGTH_SHORT).show();

                    } else {
                        new SaveProfileData().execute();
                        //
                        //Log.d("detailsx", nameString + " " + pinString + " " + withDrawlMethod + " " + withDrawlNumberString);
                    }


                } else {
                    Toast.makeText(context, "ইন্টারনেট কানেকশন নেই।", Toast.LENGTH_SHORT).show();
                }


            }
        });

        //skipButton = findViewById(R.id.skipNowButton);
//        skipButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (successfully) {
//
//                    localData.setUserID(userID);
//                    localData.setLoggedIn(true);
//
//                    Intent intent = new Intent(context, MainActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    Toast.makeText(context, "Something went wrong! ", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


    }




    // ######################### --------------------------------------#############################

    private class SaveProfileData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ProfileSetUp.this, "Saving Your Data", "Hold on a second!", true, false);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetDataFromUrl1 getData = new GetDataFromUrl1();
            String response = null;
            try {
                response = getData.run(GATE_WAY + UPDATE_PROFILE);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (ProfileSetUp.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                    return;
                }
            }

            if (result != null) {
                if (!result.isEmpty()) {


                    try {
                        JSONObject mainJson = new JSONObject(result);
                        statusCode = mainJson.getInt(STATUS);


                        if (statusCode == STATUS_OKAY) {

                            localData.setLoggedIn(true);
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (statusCode == STATUS_ERROR) {
                            Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();

                        } else if (statusCode == STATUS_ACCOUNT_ERROR) {
                            Toast.makeText(context, "আপনার অ্যাকাউন্টে সমস্যা হয়েছে।\nবিস্তারিত জানতে ফেসবুক গ্রুপে পোস্ট দিন।", Toast.LENGTH_SHORT).show();

                            LocalData localData = new LocalData(getApplicationContext());
                            localData.setLoggedIn(false);
                            FirebaseAuth.getInstance().signOut();

                        } else if (statusCode == STATUS_SERVER_ERROR) {
                            Toast.makeText(context, mainJson.getString(MESSAGE), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }


            if (!ProfileSetUp.this.isFinishing() && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    private class GetDataFromUrl1 {

        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {


            RequestBody formBody = new FormBody.Builder()
                    .add(USER_ID, localData.getUserID() + "")
                    .add(IDENTIFIER, LocalData.getIdentifier(getApplicationContext()))
                    .add(NAME, nameString)
                    .add(PIN, pinString)
                    .add(FIREBASE_ID, SharedPrefManager.getInstance(context).getDeviceToken())
                    .add(WITHDRAW_METHOD, withDrawlMethod)
                    .add(WITHDRAW_NO, withDrawlNumberString)
                    .build();

            //Log.d("details", localData.getUserID() + " " + LocalData.getIdentifier(getApplicationContext()) + " " + nameString + " " + pinString + " " + withDrawlMethod + " " + withDrawlNumberString);

            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

    }

    public static boolean isNull(String s) {

        return s == null || s.trim().isEmpty();
    }
}
