package xyz.cruxlab.pocketmoney_edited;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

/**
 * Created by Sudarshan 3.7.18
 */

public class LocalData {

    private static final String APP_SHARED_PREFS = "adsData";

    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private static final String nameString = "nameString";
    private static final String firstTime = "firstTime";
    private static final String loggedIn = "loggedIn";
    private static final String gmailAddress = "gmailAddress";
    private static final String photoURL = "photoURL";
    private static final String userID = "userID";
    private static final String dateTime = "dateTime";
    private static final String savedDate = "savedDate";

    private static final String adShowRemain = "adShowRemain";
    private static final String MAX_ADS_SESSION = "MAX_ADS_SESSION";

    private static final String refCode = "refCode";
    private static final String accCreated = "accCreated";
    private static final String withDNumber = "withDNumber";



    private static final String withdrawal_method = "withdrawal_method";
    private static final String ac_status = "ac_status";
    private static final String badge = "badge";


    private static final String ads1 = "ads1";
    private static final String ads2 = "ads2";


    private static final String sec30 = "sec30";
    private static final String min30 = "min30";


    private static final String name = "name";
    private static final String pin = "pin";
    private static final String cPoint = "cPoint";
    private static final String tPoint = "tPoint";


    private static final String appURl = "appURl";


    public LocalData(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public static String getIdentifier(Context context) {

        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    // is first time
    public boolean getFirstTime() {
        return appSharedPrefs.getBoolean(firstTime, false);
    }

    public void setFirstTime() {
        prefsEditor.putBoolean(firstTime, true);
        prefsEditor.commit();
    }

    public boolean getLoggedIn() {
        return appSharedPrefs.getBoolean(loggedIn, false);
    }

    public void setLoggedIn(boolean status) {
        prefsEditor.putBoolean(loggedIn, status);
        prefsEditor.commit();
    }


    // ads unite
    public String getAds1() {
        return appSharedPrefs.getString(ads1, "");
    }

    public void setAds1(String ad) {
        prefsEditor.putString(ads1, ad);
        prefsEditor.commit();
    }

    public String getAds2() {
        return appSharedPrefs.getString(ads2, "");
    }

    public void setAds2(String ad) {
        prefsEditor.putString(ads2, ad);
        prefsEditor.commit();
    }


    // user ID

    public int getUserID() {

        return appSharedPrefs.getInt(userID, 0); // todo
    }

    public void setUserID(int id) {
        prefsEditor.putInt(userID, id);
        prefsEditor.commit();
    }


    // date
    public long getTime() {

        return appSharedPrefs.getLong(dateTime, 0);
    }

    public void setTime(long d) {
        prefsEditor.putLong(dateTime, d / 1000);
        prefsEditor.commit();
    }

    //time 30 min
//    public long getMin30() {
//
//        return appSharedPrefs.getLong(min30, 0);
//    }
//
//    public void setMin30(long d) {
//        prefsEditor.putLong(min30, d / 1000);
//        prefsEditor.commit();
//    }

    //sec 30 second
    public long getSec30() {

        return appSharedPrefs.getLong(sec30, 0);
    }

    public void setSec30(long d) {
        prefsEditor.putLong(sec30, d / 1000);
        prefsEditor.commit();
    }


    // name
    public String getName() {
        return appSharedPrefs.getString(nameString, "");
    }

    public void setName(String name) {
        prefsEditor.putString(nameString, name);
        prefsEditor.commit();
    }

    // withdrawal number
    public String getWithDNumber() {
        return appSharedPrefs.getString(withDNumber, "");
    }

    public void setWithDNumber(String no) {
        prefsEditor.putString(withDNumber, no);
        prefsEditor.commit();
    }


    /// photo // photo url

    public void setPhotoUrl(String name) {
        prefsEditor.putString(photoURL, name);
        prefsEditor.commit();
    }

    public String getPhotoUrl() {
        return appSharedPrefs.getString(photoURL, "");
    }


    // email
    public String getGmail() {
        return appSharedPrefs.getString(gmailAddress, "");
    }

    public void setGmail(String name) {
        prefsEditor.putString(gmailAddress, name);
        prefsEditor.commit();
    }


    // pin
    public String getPin() {
        return appSharedPrefs.getString(pin, "");
    }

    public void setPin(String pin1) {
        prefsEditor.putString(pin, pin1);
        prefsEditor.commit();
    }

    // acc Created
    public String getAccCreated() {
        return appSharedPrefs.getString(accCreated, "2018-07-11");
    }

    public void setAccCreated(String date) {
        prefsEditor.putString(accCreated, date);
        prefsEditor.commit();
    }

    // acc Created
    public String getRefCode() {
        return appSharedPrefs.getString(refCode, "");
    }

    public void setRefCode(String ref) {
        prefsEditor.putString(refCode, ref);
        prefsEditor.commit();
    }

    //.............................
    // withdrawal method
    public String getWithdrawal_method() {
        return appSharedPrefs.getString(withdrawal_method, "");
    }

    public void setWithdrawal_method(String method) {
        prefsEditor.putString(withdrawal_method, method);
        prefsEditor.commit();
    }

    // ac_status
    public int getAccountStatus() {
        return appSharedPrefs.getInt(ac_status, 0);
    }

    public void setAccountStatus(int status) {
        prefsEditor.putInt(ac_status, status);
        prefsEditor.commit();
    }

    //badge
    public String getBadge() {
        return appSharedPrefs.getString(badge, "");
    }

    public void setBadge(String b) {
        prefsEditor.putString(badge, b);
        prefsEditor.commit();
    }


    // save today
    public String getSavedDate() {
        return appSharedPrefs.getString(savedDate, "2018-07-20");
    }

    public void setSavedDate(String date) {
        prefsEditor.putString(savedDate, date);
        prefsEditor.commit();
    }


    // max ads session

    public int getMaxAdsSession() {
        return appSharedPrefs.getInt(MAX_ADS_SESSION, 9);
    }

    public void setMaxAdsSession(int sessionNo) {
        prefsEditor.putInt(MAX_ADS_SESSION, sessionNo);
        prefsEditor.commit();
    }


    //  counter remain

    // todo
    public int getRemainDailySession() {
        return appSharedPrefs.getInt(adShowRemain, 1);
    }

    public void setRemainDailySession(int number) {
        prefsEditor.putInt(adShowRemain, number);
        prefsEditor.commit();
    }


    // current point

    public String getcPoint() {
        return appSharedPrefs.getString(cPoint, "0");
    }

    public void setcPoint(String s) {
        prefsEditor.putString(cPoint, s);
        prefsEditor.commit();
    }

    // current point

    public String getTodaysPoint() {
        return appSharedPrefs.getString(tPoint, "0");
    }

    public void setTodaysPoint(String s) {
        prefsEditor.putString(tPoint, s);
        prefsEditor.commit();
    }

    // current point

    public String getTPoint() {
        return appSharedPrefs.getString("totalPoint", "0");
    }

    public void setTPoint(String s) {
        prefsEditor.putString("totalPoint", s);
        prefsEditor.commit();
    }


    // AppUrl
    public String getAppURL() {
        return appSharedPrefs.getString(appURl, "");
    }

    public void setAppURL(String name) {
        prefsEditor.putString(appURl, name);
        prefsEditor.commit();
    }

    public void reset() {
        prefsEditor.clear();
        prefsEditor.commit();

    }

}
